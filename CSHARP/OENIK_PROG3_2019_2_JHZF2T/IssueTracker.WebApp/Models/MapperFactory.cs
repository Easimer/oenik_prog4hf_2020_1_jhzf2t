﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace Net.Easimer.Prog3.IssueTracker.WebApp.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ProjectInfo, ProjectModel>()
                .ForMember(dst => dst.Id, map => map.MapFrom(src => src.ID))
                .ForMember(dst => dst.Name, map => map.MapFrom(src => src.Name))
                .ForMember(dst => dst.Owner, map => map.MapFrom(src => src.Owner))
                .ForMember(dst => dst.Homepage, map => map.MapFrom(src => src.Homepage))
                .ForMember(dst => dst.CreationDate, map => map.MapFrom(src => src.CreationDate))
                .ForMember(dst => dst.Parent, map => map.MapFrom(src => src.Parent))
                .ForMember(dst => dst.ParentProjectName, map => map.MapFrom(src => src.ParentRef != null ? src.ParentRef.Name : ""))
                .ForMember(dst => dst.OwnerName, map => map.MapFrom(src => src.OwnerRef != null ? src.OwnerRef.Username : ""));
            });

            return config.CreateMapper();
        }       
    }
}