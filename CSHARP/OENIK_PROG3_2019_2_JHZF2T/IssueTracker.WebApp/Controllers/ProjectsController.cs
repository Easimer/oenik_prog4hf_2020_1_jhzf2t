﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Net.Easimer.Prog3.Database;
using Net.Easimer.Prog3.IssueTracker.WebApp.Models;

namespace Net.Easimer.Prog3.IssueTracker.WebApp.Controllers
{
    public class ProjectsController : Controller
    {
        private IIssueTracker issueTracker;
        private IProjectManagement projMgmt;
        private IUserManagement userMgmt;
        private IMapper mapper;
        private ProjectsViewModel vm;

        public ProjectsController()
        {
            IDataAccess datacc = new DataAccessImpl();
            issueTracker = new IssueTracker(datacc);
            projMgmt = issueTracker.GetProjectManagement();
            userMgmt = issueTracker.GetUserManagement();

            mapper = MapperFactory.CreateMapper();

            vm = new ProjectsViewModel();
            vm.Edited = new ProjectModel();
            vm.Projects = projMgmt.GetAllProjects().Select(p => mapper.Map<ProjectInfo, ProjectModel>(p)).ToList();
        }

        private ProjectModel GetById(int id)
        {
            return mapper.Map<ProjectInfo, ProjectModel>(projMgmt.GetByID(id));
        }

        // GET: Projects
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            ViewData["editResult"] = string.Empty;
            return View("ProjectIndex", vm);
        }

        public ActionResult Details(int id)
        {
            return View("ProjectDetails", GetById(id));
        }

        public ActionResult Delete(int id)
        {
            var res = projMgmt.DeleteByID(id);
            TempData["editResult"] = res ? "Delete OK" : "Delete fail";
            return RedirectToAction(nameof(Index));
        }

        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.Edited = GetById(id);
            return View("ProjectIndex", vm);
        }

        [HttpPost]
        public ActionResult Edit(ProjectModel proj, string editAction)
        {
            if(ModelState.IsValid && proj != null)
            {
                bool res = false;
                switch(editAction)
                {
                    case "AddNew":
                        res = projMgmt.CreateProject(proj.Id, proj.Name, proj.Homepage, null) != null;
                        break;
                    case "Edit":
                        var oldProj  = projMgmt.GetByID(proj.Id);
                        oldProj.Name = proj.Name;
                        oldProj.Homepage = proj.Homepage;
                        oldProj.Owner = proj.Owner;
                        projMgmt.Modify(proj.Id, oldProj);
                        res = true;
                        break;
                }

                ViewData["editResult"] = res ? "Edit OK" : "Edit fail";

                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.Edited = proj;
                return View("ProjectIndex", vm);
            }
        }
    }
}