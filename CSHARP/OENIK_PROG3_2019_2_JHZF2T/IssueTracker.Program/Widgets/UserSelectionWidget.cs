﻿// <copyright file="UserSelectionWidget.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System;
    using System.Collections.Generic;
    using Net.Easimer.Prog3.IssueTracker;

    /// <summary>
    /// User selection widget.
    /// </summary>
    [Selector(typeof(UserInfo))]
    internal class UserSelectionWidget : BaseSelectionWidget
    {
        private IUserManagement userMgmt;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserSelectionWidget"/> class.
        /// </summary>
        /// <param name="issueTracker">User management interface.</param>
        public UserSelectionWidget(IIssueTracker issueTracker)
            : base(issueTracker)
        {
            this.userMgmt = issueTracker.GetUserManagement();
        }

        /// <inheritdoc/>
        protected override IEnumerable<string> GetColumnNames()
        {
            return new string[]
            {
                "#",
                "Username",
                "First name",
                "Last name",
                "Email",
                "Tel.",
            };
        }

        /// <inheritdoc/>
        protected override int GetColumnWidth(int colIdx)
        {
            switch (colIdx)
            {
                case 0: return 4;
                case 4: return 24;
                default: return 12;
            }
        }

        /// <inheritdoc/>
        protected override IEnumerable<Tuple<object[], object>> GetTableData()
        {
            foreach (var user in this.userMgmt.GetAllUsers())
            {
                var ret = new Tuple<object[], object>(
                    new object[]
                    {
                        user.ID,
                        user.Username,
                        user.FirstName,
                        user.LastName,
                        user.EmailAddr,
                        user.Telephone,
                    }, user);

                yield return ret;
            }
        }
    }
}
