﻿// <copyright file="BaseManagementException.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker
{
    using System;

    /// <summary>
    /// Base management exception.
    /// </summary>
    /// <typeparam name="T">Entity type.</typeparam>
    [Serializable]
    public class BaseManagementException<T> : Exception
        where T : class
    {
        private T relatedEntity;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseManagementException{T}"/> class.
        /// </summary>
        public BaseManagementException()
            : this(string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseManagementException{T}"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        public BaseManagementException(string message)
            : this(message, null, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseManagementException{T}"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <param name="relatedEntity">Entity related to the incident.</param>
        public BaseManagementException(string message, T relatedEntity)
            : this(message, null, relatedEntity)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseManagementException{T}"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <param name="innerException">Inner exception.</param>
        public BaseManagementException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseManagementException{T}"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <param name="innerException">Inner exception.</param>
        /// <param name="relatedEntity">Entity related to the incident.</param>
        public BaseManagementException(string message, Exception innerException, T relatedEntity)
            : base(message, innerException)
        {
            this.relatedEntity = relatedEntity;
        }

        /// <summary>
        /// Gets the entity related to the incident.
        /// </summary>
        public T RelatedEntity => this.relatedEntity;
    }
}
