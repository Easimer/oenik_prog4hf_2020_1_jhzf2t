var searchData=
[
  ['parent_272',['Parent',['../class_net_1_1_easimer_1_1_prog3_1_1_database_1_1_data_1_1_project.html#a01c2481721c2fda1a9831858a394939a',1,'Net::Easimer::Prog3::Database::Data::Project']]],
  ['parentref_273',['ParentRef',['../class_net_1_1_easimer_1_1_prog3_1_1_database_1_1_data_1_1_project.html#abd8ccef787e1e36340f621c5126da2fe',1,'Net.Easimer.Prog3.Database.Data.Project.ParentRef()'],['../class_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_project_info.html#ae7f03f2111e6c59ef66deaec7764c026',1,'Net.Easimer.Prog3.IssueTracker.ProjectInfo.ParentRef()']]],
  ['project_274',['Project',['../class_net_1_1_easimer_1_1_prog3_1_1_database_1_1_data_1_1_issue.html#ae4d5b2648e88828bdff8181671e8b159',1,'Net::Easimer::Prog3::Database::Data::Issue']]],
  ['projectref_275',['ProjectRef',['../class_net_1_1_easimer_1_1_prog3_1_1_database_1_1_data_1_1_issue.html#aceb7513c24c2e938779b51d3d01ea52a',1,'Net.Easimer.Prog3.Database.Data.Issue.ProjectRef()'],['../class_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_issue_info.html#a30b85002d677c8c2cbb99aeda507fc09',1,'Net.Easimer.Prog3.IssueTracker.IssueInfo.ProjectRef()']]],
  ['projects_276',['Projects',['../interface_net_1_1_easimer_1_1_prog3_1_1_database_1_1_data_1_1_i_issue_tracker_model.html#a27ccc8567b93d55f9e9bb6a62b4b38c5',1,'Net.Easimer.Prog3.Database.Data.IIssueTrackerModel.Projects()'],['../class_net_1_1_easimer_1_1_prog3_1_1_database_1_1_data_1_1_issue_tracker_model.html#ad26d72a73358c6efdedc486855f2dfb3',1,'Net.Easimer.Prog3.Database.Data.IssueTrackerModel.Projects()']]]
];
