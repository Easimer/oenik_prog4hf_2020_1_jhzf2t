﻿// <copyright file="IIssueManagement.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Issue management interface.
    /// </summary>
    public interface IIssueManagement
    {
        /// <summary>
        /// Create a new issue related to a project and optionally assigned to a user.
        /// </summary>
        /// <param name="projectID">Project ID.</param>
        /// <param name="title">Title/short description of the issue.</param>
        /// <param name="assignedTo">ID of the user to assign this issue to.</param>
        /// <returns>Information about the resulting issue.</returns>
        /// <exception cref="IssueManagementException">Thrown on error.</exception>
        IssueInfo CreateNewIssue(int projectID, string title, int? assignedTo);

        /// <summary>
        /// Assign an issue to a user.
        /// </summary>
        /// <param name="issueID">Issue ID.</param>
        /// <param name="userID">User ID.</param>
        /// <returns>Information about the issue.</returns>
        /// <exception cref="IssueManagementException">Thrown when issue with such an ID does not exist.</exception>
        /// <exception cref="UserManagementException">Thrown when user with such an ID does not exist.</exception>
        IssueInfo AssignIssue(int issueID, int userID);

        /// <summary>
        /// Close an issue.
        /// </summary>
        /// <param name="issueID">Issue ID.</param>
        /// <returns>Information about the issue.</returns>
        /// <exception cref="IssueManagementException">Thrown when issue with such an ID does not exist.</exception>
        IssueInfo CloseIssue(int issueID);

        /// <summary>
        /// Delete an issue.
        /// </summary>
        /// <param name="issueID">Issue ID.</param>
        /// <exception cref="IssueManagementException">Thrown when issue with such an ID does not exist or it exists but the operation has failed.</exception>
        void DeleteIssue(int issueID);

        /// <summary>
        /// Find an issue by ID.
        /// </summary>
        /// <param name="issueID">Issue ID.</param>
        /// <returns>Information about the issue.</returns>
        /// <exception cref="IssueManagementException">Thrown when issue with such an ID does not exist.</exception>
        IssueInfo GetById(int issueID);

        /// <summary>
        /// Get all issues.
        /// </summary>
        /// <returns>An IQueryable of all issues.</returns>
        IQueryable<IssueInfo> GetAllIssues();

        /// <summary>
        /// Get all issues related to a project.
        /// </summary>
        /// <param name="projectID">Project ID.</param>
        /// <returns>An IQueryable of all issues related to a project.</returns>
        IQueryable<IssueInfo> GetAllIssues(int projectID);

        /// <summary>
        /// Searches for issues with <paramref name="s"/> in their name and
        /// returns them.
        /// </summary>
        /// <param name="s">Search parameter.</param>
        /// <param name="projectID">Project ID.</param>
        /// <returns>Issues matching the search parameter.</returns>
        IEnumerable<IssueInfo> SearchForIssues(string s, int? projectID = null);
    }
}
