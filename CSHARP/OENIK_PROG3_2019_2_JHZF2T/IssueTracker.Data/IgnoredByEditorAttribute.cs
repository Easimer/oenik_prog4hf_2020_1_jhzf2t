﻿// <copyright file="IgnoredByEditorAttribute.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database
{
    using System;

    /// <summary>
    /// A field marked by this will be ignored by Database.Reflection editors.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = true)]
    public class IgnoredByEditorAttribute : Attribute
    {
    }
}
