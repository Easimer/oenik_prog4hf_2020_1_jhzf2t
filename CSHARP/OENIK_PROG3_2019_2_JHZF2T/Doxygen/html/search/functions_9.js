var searchData=
[
  ['project_229',['Project',['../class_net_1_1_easimer_1_1_prog3_1_1_database_1_1_data_1_1_project.html#a3babe4a37ffaa3853f9c1770c5a10141',1,'Net.Easimer.Prog3.Database.Data.Project.Project()'],['../class_net_1_1_easimer_1_1_prog3_1_1_database_1_1_data_1_1_project.html#a469c94406797011fa95e30ef9121889f',1,'Net.Easimer.Prog3.Database.Data.Project.Project(Project other)']]],
  ['projectinfo_230',['ProjectInfo',['../class_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_project_info.html#a21cda1c97b47f77e7ee0d1a328c27310',1,'Net.Easimer.Prog3.IssueTracker.ProjectInfo.ProjectInfo()'],['../class_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_project_info.html#a4b9756d55af49bb2c2b636751ec9b287',1,'Net.Easimer.Prog3.IssueTracker.ProjectInfo.ProjectInfo(IProjects projects, IUsers users, Project prj)']]],
  ['projectmanagementexception_231',['ProjectManagementException',['../class_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_project_management_exception.html#a0466242c3676cbdb7fff86f9e9037a8b',1,'Net::Easimer::Prog3::IssueTracker::ProjectManagementException']]],
  ['projectstatistics_232',['ProjectStatistics',['../struct_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_project_statistics.html#ab29dd8778a8e1b89f80476251bf8206f',1,'Net::Easimer::Prog3::IssueTracker::ProjectStatistics']]]
];
