package net.easimer.prog3.complaints.generator;

import java.util.Random;

/**
 *
 * @author danielm
 */
public class StatementUserProblem implements BaseStatement {
    private String m_problem;
    private static String[] problems = new String[] {
        "hibát ír ki", "nem csinál semmit", "összeomlik a program"
    };
    
    public StatementUserProblem(Random rnd) {
        m_problem = problems[rnd.nextInt(problems.length)];
    }
    
    @Override
    public String getStatementText() {
        return m_problem;
    }
    
}
