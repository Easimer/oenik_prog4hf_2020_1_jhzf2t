﻿// <copyright file="Program.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System;
    using Net.Easimer.Prog3.Database;
    using Net.Easimer.Prog3.IssueTracker;

    /// <summary>
    /// Main program class.
    /// </summary>
    internal class Program
    {
        private static void ResizeWindow()
        {
            int newWidth = Console.LargestWindowWidth > 180 ? 180 : Console.LargestWindowWidth;
            int newHeight = Console.LargestWindowHeight > 80 ? 80 : Console.LargestWindowHeight;
            newWidth = newWidth > Console.LargestWindowWidth ? Console.LargestWindowWidth : newWidth;
            newHeight = newHeight > Console.LargestWindowHeight ? Console.LargestWindowHeight : newHeight;
            Console.SetWindowSize(newWidth, newHeight);
        }

        /// <summary>
        /// Main program subroutine.
        /// </summary>
        /// <param name="args">Program arguments.</param>
        private static void Main(string[] args)
        {
            ResizeWindow();
            IDataAccess datacc = new DataAccessImpl();
            IIssueTracker it = new IssueTracker(datacc);
            Client c = new Client(it);

            c.Loop();
        }
    }
}
