package net.easimer.prog3;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author danielm
 */
@WebServlet(name = "Homepage", urlPatterns = {"/"})
public class Index extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.sendRedirect("https://bitbucket.org/Easimer/oenik_prog3_2019_2_jhzf2t/src/master/JAVA/README.md");
    }
}
