﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace IssueTracker.WebConsole
{
    class Program
    {
        public class ProjectModel
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public int? Owner { get; set; }

            public string OwnerName { get; set; }

            public string Homepage { get; set; }

            public DateTime CreationDate { get; set; }

            public int? Parent { get; set; }

            public string ParentProjectName { get; set; }

            public override string ToString()
            {
                return $"({Id}, {Name}, {Owner}, {Homepage}, {CreationDate}, {Parent})";
            }
        }

        private static string host = "http://localhost:65413/";
        private static string baseUrl = host + "api/ProjectsAPI/";

        static IEnumerable<ProjectModel> All(HttpClient client)
        {
            var response = client.GetStringAsync(baseUrl + "all/").Result;
            return JsonConvert.DeserializeObject<IEnumerable<ProjectModel>>(response);
        }

        static void ListAll(HttpClient client)
        {
            var list = All(client);
            foreach(var proj in list)
            {
                Console.WriteLine(proj);
            }
        }

        static string TestAdd(HttpClient client)
        {
            var postData = new Dictionary<string, string>();
            postData.Add(nameof(ProjectModel.Id), "4096");
            postData.Add(nameof(ProjectModel.Name), "WebClient Test Project");
            postData.Add(nameof(ProjectModel.Owner), "1");
            postData.Add(nameof(ProjectModel.Parent), "1");
            postData.Add(nameof(ProjectModel.Homepage), "about:blank");

            return client.PostAsync(baseUrl + "add/", new FormUrlEncodedContent(postData))
                .Result.Content
                .ReadAsStringAsync().Result;
        }

        static string TestModify(HttpClient client)
        {
            var list = All(client);
            var testProj = list.Single(x => x.Name == "WebClient Test Project");

            var postData = new Dictionary<string, string>();
            postData.Add(nameof(ProjectModel.Id), testProj.Id.ToString());
            postData.Add(nameof(ProjectModel.Name), "WebClient Test Project");
            postData.Add(nameof(ProjectModel.Owner), "2");
            postData.Add(nameof(ProjectModel.Parent), "1");
            postData.Add(nameof(ProjectModel.Homepage), "about:blank");

            return client.PostAsync(baseUrl + "edit/", new FormUrlEncodedContent(postData))
                .Result.Content
                .ReadAsStringAsync().Result;
        }

        static string TestDelete(HttpClient client)
        {
            var list = All(client);
            var testProj = list.Single(x => x.Name == "WebClient Test Project");

            return client.GetAsync(baseUrl + "del/" + testProj.Id).Result.Content.ReadAsStringAsync().Result;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("hi");

            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromSeconds(120);

                ListAll(client);

                var addRes = TestAdd(client);
                Console.WriteLine($"TestAdd response: {addRes}");
                ListAll(client);
                Console.ReadLine();

                var modRes = TestModify(client);
                Console.WriteLine($"TestModify response: {modRes}");
                ListAll(client);
                Console.ReadLine();

                var delRes = TestDelete(client);
                Console.WriteLine($"TestDelete response: {delRes}");
                ListAll(client);
                Console.ReadLine();
            }

            Console.ReadLine();
        }
    }
}
