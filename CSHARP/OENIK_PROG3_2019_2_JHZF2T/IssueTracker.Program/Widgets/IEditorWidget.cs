﻿// <copyright file="IEditorWidget.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    /// <summary>
    /// Editor widget interface.
    /// </summary>
    internal interface IEditorWidget
    {
        /// <summary>
        /// Gets or sets the object being edited or selected.
        /// </summary>
        object Edited { get; set; }

        /// <summary>
        /// Run widget logic.
        /// </summary>
        /// <returns>Returns a value indicating whether any edited value(s) are present.</returns>
        bool Update();

        /// <summary>
        /// Invalidates the cached table data.
        /// </summary>
        void InvalidateCache();
    }
}
