﻿// <copyright file="Project.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database.Data
{
    using System;

    /// <summary>
    /// Project entity.
    /// </summary>
    public class Project : IEntityWithID
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Project"/> class.
        /// </summary>
        public Project()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Project"/> class.
        /// </summary>
        /// <param name="other">Other instance.</param>
        public Project(Project other)
        {
            this.ID = other.ID;
            this.Owner = other.Owner;
            this.Name = other.Name;
            this.Homepage = other.Homepage;
            this.CreationDate = other.CreationDate;
            this.Parent = other.Parent;
            this.Descript = other.Descript;
            this.Lang = other.Lang;
            this.ParentRef = other.ParentRef;
            this.OwnerRef = other.OwnerRef;
        }

        /// <summary>
        /// Gets or sets the project identifier.
        /// </summary>
        [IgnoredByEditor] // NOTE(danielm): IDs are automatically generated.
        public virtual int ID { get; set; }

        /// <summary>
        /// Gets or sets the owner user.
        /// </summary>
        [IgnoredByEditor]
        public int? Owner { get; set; }

        /// <summary>
        /// Gets or sets the project's name.
        /// </summary>
        [EditorRequired]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the project's homepage.
        /// </summary>
        public string Homepage { get; set; }

        /// <summary>
        /// Gets or sets the date of creation.
        /// </summary>
        [EditorDescription("Date of creation")]
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Gets or sets the parent project.
        /// </summary>
        [IgnoredByEditor]
        public int? Parent { get; set; }

        /// <summary>
        /// Gets or sets the description of this project.
        /// </summary>
        [EditorDescription("Description")]
        public string Descript { get; set; }

        /// <summary>
        /// Gets or sets the name of the language this project is written in.
        /// </summary>
        [EditorRequired]
        [EditorDescription("Language")]
        public string Lang { get; set; }

        /// <summary>
        /// Gets or sets the reference to the parent.
        /// </summary>
        [IgnoredByEditor] // NOTE(danielm): hidden because ProjectInfo will hide this prop
        public virtual Project ParentRef { get; set; }

        /// <summary>
        /// Gets or sets the reference to the owning user.
        /// </summary>
        [IgnoredByEditor] // NOTE(danielm): hidden because ProjectInfo will hide this prop
        public virtual User OwnerRef { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            bool ret = false;
            if (obj is Project other)
            {
                ret = this.ID.Equals(other.ID);
                ret &= this.Name.Equals(other.Name);
                ret &= this.Homepage.Equals(other.Homepage);
                ret &= this.CreationDate.Equals(other.CreationDate);
                ret &= this.Parent.Equals(other.Parent);
                ret &= this.Owner.Equals(other.Owner);
            }

            return ret;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            int ret = 13;
            ret = (ret * 7) + this.ID.GetHashCode();
            ret = (ret * 7) + this.Name.GetHashCode();
            ret = (ret * 7) + this.Homepage.GetHashCode();
            ret = (ret * 7) + this.CreationDate.GetHashCode();
            ret = (ret * 7) + this.Parent.GetHashCode();
            ret = (ret * 7) + this.Owner.GetHashCode();
            return ret;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return string.Format(
                "#{0} '{1}' '{2}' '{3}' (owned by '{4}') (parent project ID #{5})",
                this.ID,
                this.Name,
                this.Homepage,
                this.CreationDate.ToLongDateString(),
                this.Owner.HasValue ? this.Owner.ToString() : "(null)",
                this.Parent.HasValue ? this.Parent.ToString() : "(null)");
        }
    }
}
