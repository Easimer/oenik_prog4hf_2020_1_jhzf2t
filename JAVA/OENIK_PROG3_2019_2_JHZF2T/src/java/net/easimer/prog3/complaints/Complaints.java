package net.easimer.prog3.complaints;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import net.easimer.prog3.complaints.generator.StatementComplaint;

/**
 *
 * @author danielm
 */
@WebServlet(name = "Customer complaints", urlPatterns = {"/GetComplaints/v1"})
public class Complaints extends HttpServlet {
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/xml;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            JAXBContext xctx;
            Marshaller marshaller;
            
            ComplaintsResponse res = new ComplaintsResponse();
            res.setComplaints(generateComplaints());
            
            try {
                xctx = JAXBContext.newInstance(ComplaintsResponse.class);
                marshaller = xctx.createMarshaller();
                marshaller.marshal(res, out);
            } catch(JAXBException ex) {
                // TODO(danielm): uh oh
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Customer complaints endpoint";
    }// </editor-fold>
    
    private ArrayList<Complaint> generateComplaints()
    {
        ArrayList<Complaint> ret = new ArrayList<>();
        Random rnd = new Random();
        
        for(int i = 0; i < 2 + rnd.nextInt(14); i++) {
            String contents = new StatementComplaint(rnd).getStatementText();
            String author = generateName(rnd);
            
            ret.add(new Complaint(author, contents));
        }
        
        return ret;
    }
    
    private static String[] lastNames = new String[] {
        "Nagy", "Kovács", "Tóth", "Szabó", "Horváth", "Varga", "Kiss",
        "Molnár", "Németh", "Farkas", "Balogh", "Papp", "Lakatos",
        "Takács", "Juhász", "Mészáros"
    };
    
    private static String[] firstNames = new String[] {
        "Bence", "Máté", "Dominik", "Marcell", "Levente", "Noel", "Ádám",
        "Hanna", "Zoé", "Anna", "Emma", "Luca", "Léna", "Zsófia",
        "Dániel", "Milán", "Dávid", "Olivér", "Zalán", "Áron", "Botond",
        "Boglárka", "Jázmin", "Lili", "Nóra", "Laura", "Maja", "Mira",
    };
    
    private String generateName(Random rnd) {
        return String.format("%s %s",
                firstNames[rnd.nextInt(firstNames.length)],
                lastNames[rnd.nextInt(lastNames.length)]);
    }
            
}
