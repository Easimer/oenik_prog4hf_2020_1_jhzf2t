﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.Easimer.Prog3.IssueTracker.WebApp.Models
{
    public class ProjectsViewModel
    {
        public IEnumerable<ProjectModel> Projects { get; set; }
        public ProjectModel Edited { get; set; }
    }
}
