package net.easimer.prog3.complaints;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielm
 */

@XmlRootElement(name = "ComplaintsResult")
class ComplaintsResponse {
    public ComplaintsResponse() {
        m_complaints = new ArrayList<>();
    }
    
    @XmlElement(name = "Complaint")
    public ArrayList<Complaint> getComplaints() {
        return m_complaints;
    }
    
    public void setComplaints(ArrayList<Complaint> newValue) {
        m_complaints = newValue;
    }

    private ArrayList<Complaint> m_complaints;
}

class Complaint {
    public Complaint() {
        m_author = "(unknown)";
        m_contents = "";
    }
    
    public Complaint(String author, String contents) {
        m_author = author;
        m_contents = contents;
    }
    
    @XmlElement(name = "Author")
    private String m_author;
    @XmlElement(name = "Contents")
    private String m_contents;
}
