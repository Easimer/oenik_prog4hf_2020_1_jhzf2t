package net.easimer.prog3.complaints.generator;

import java.util.Random;

/**
 *
 * @author danielm
 */
public class StatementDoesSomethingElse implements BaseStatement {
    private BaseStatement m_activity, m_problem;
    
    public StatementDoesSomethingElse(Random rnd) {
        m_activity = new StatementUserActivity(rnd);
        m_problem = new StatementUserProblem(rnd);
    }
    
    @Override
    public String getStatementText() {
        return String.format("Ha megpróbálok %s, akkor helyette %s.",
                m_activity.getStatementText(), m_problem.getStatementText());
    }
    
}
