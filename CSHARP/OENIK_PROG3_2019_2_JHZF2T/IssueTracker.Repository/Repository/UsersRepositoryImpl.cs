﻿// <copyright file="UsersRepositoryImpl.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Net.Easimer.Prog3.Database.Data;

    /// <summary>
    /// Users repository implementation.
    /// </summary>
    internal class UsersRepositoryImpl : BaseRepositoryImpl<User>, IUsers
    {
        private IIssueTrackerModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="UsersRepositoryImpl"/> class.
        /// </summary>
        /// <param name="model">Issue Tracker model.</param>
        public UsersRepositoryImpl(IIssueTrackerModel model)
            : base(model.Users)
        {
            this.model = model;
        }

        /// <inheritdoc/>
        protected override int Flush()
        {
            try
            {
                return this.model.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                throw new AggregateException(
                    ex.EntityValidationErrors
                        .Select(
                            x => x.ValidationErrors
                                .Select(
                                    y => new RepositoryErrorException(y.ErrorMessage)))
                                .SelectMany(x => x) // flatten
                                .ToArray());
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
            {
                List<Exception> rex = new List<Exception>();
                Exception err = ex;
                while (err != null)
                {
                    rex.Add(err);
                    err = err.InnerException;
                }

                throw new AggregateException(rex);
            }
        }
    }
}
