package net.easimer.prog3.complaints.generator;

/**
 *
 * @author danielm
 */
public interface BaseStatement {
    public String getStatementText();
}
