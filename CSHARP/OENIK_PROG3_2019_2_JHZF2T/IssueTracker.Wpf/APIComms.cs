﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace IssueTracker.Wpf
{
    internal class APIComms : IAPIComms
    {
        private HttpClient client;
        private string host;
        private string baseProjects;

        /// <summary>
        /// Connect to a remote API endpoint.
        /// </summary>
        /// <param name="host">A URL like "http://localhost:65413/".</param>
        public APIComms(string host)
        {
            this.client = new HttpClient();
            this.host = host;
            this.baseProjects = "api/ProjectsAPI/";
        }

        public IEnumerable<ProjectViewModel> GetAllProjects()
        {
            var response = this.client.GetStringAsync(this.host + this.baseProjects + "all/")
                .Result;
            return JsonConvert.DeserializeObject<IEnumerable<ProjectViewModel>>(response);
        }

        public void Delete(ProjectViewModel project)
        {
            var response = client.GetStringAsync(this.host + this.baseProjects + "del/" + project.Id)
                .Result;
            var obj = JObject.Parse(response);

            var success = obj["Result"].ToObject<bool>();
            BroadcastResult(success);
        }

        public bool Edit(ProjectViewModel project, bool isEditing)
        {
            if (project == null)
            {
                return false;
            }

            var url = this.host + this.baseProjects + (isEditing ? "edit/" : "add/");
            var postData = new Dictionary<string, string>();

            postData.Add(nameof(ProjectViewModel.Name), project.Name);
            postData.Add(nameof(ProjectViewModel.Owner), project.Owner.ToString());
            postData.Add(nameof(ProjectViewModel.Parent), project.Parent.ToString());
            postData.Add(nameof(ProjectViewModel.Homepage), project.Homepage);

            if (isEditing)
            {
                postData.Add(nameof(ProjectViewModel.Id), project.Id.ToString());
            }

            var response = client.PostAsync(url, new FormUrlEncodedContent(postData))
                .Result.Content
                .ReadAsStringAsync().Result;

            var obj = JObject.Parse(response);
            var success = obj["Result"].ToObject<bool>();
            BroadcastResult(success);
            return success;
        }

        private void BroadcastResult(bool result)
        {
            Messenger.Default.Send(result ? "Operation succeeded" : "Operation failed");
        }
    }
}
