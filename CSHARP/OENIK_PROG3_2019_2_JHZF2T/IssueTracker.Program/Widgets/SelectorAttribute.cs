﻿// <copyright file="SelectorAttribute.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Net.Easimer.Prog3.IssueTracker;

    /// <summary>
    /// Attribute that marks a class as a selection widget for an entity type.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    internal class SelectorAttribute : Attribute
    {
        private static Dictionary<Type, ConstructorInfo> widgetCache = new Dictionary<Type, ConstructorInfo>();
        private Type selectionWidgetType;

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectorAttribute"/> class.
        /// </summary>
        /// <param name="t">Type of the entity.</param>
        public SelectorAttribute(Type t)
        {
            this.selectionWidgetType = t;
        }

        /// <summary>
        /// TODO(danielm): .
        /// </summary>
        /// <param name="t">Type.</param>
        /// <param name="issueTracker">Issue tracker interface.</param>
        /// <returns>Widget entity list.</returns>
        public static BaseSelectionWidget GetSelectionScreenForType(Type t, IIssueTracker issueTracker)
        {
            BaseSelectionWidget ret = null;

            if (t != null)
            {
                ConstructorInfo ctor = null;

                var args = new object[]
                {
                    issueTracker,
                };
                if (widgetCache.ContainsKey(t))
                {
                    ctor = widgetCache[t];
                }
                else
                {
                    var assembly = Assembly.GetExecutingAssembly();
                    foreach (var type in assembly.GetTypes())
                    {
                        var attr = (SelectorAttribute)type.GetCustomAttribute(typeof(SelectorAttribute));
                        if (attr != null && attr.selectionWidgetType.Equals(t))
                        {
                            var types = new Type[]
                            {
                                typeof(IIssueTracker),
                            };

                            ctor = type.GetConstructor(types);
                        }
                    }
                }

                if (ctor != null)
                {
                    ret = ctor.Invoke(args) as BaseSelectionWidget;
                }
            }

            return ret;
        }
    }
}
