package net.easimer.prog3.complaints.generator;

import java.util.Random;

/**
 *
 * @author easimer
 */
public class StatementAttempted implements BaseStatement {
    private BaseStatement m_activity, m_problem;
    
    public StatementAttempted(Random rnd) {
        m_activity = new StatementUserActivity(rnd);
        m_problem = new StatementUserProblem(rnd);
    }
    
    @Override
    public String getStatementText() {
        return String.format("Próbáltam %s, de %s.",
                m_activity.getStatementText(), m_problem.getStatementText());
    }
}
