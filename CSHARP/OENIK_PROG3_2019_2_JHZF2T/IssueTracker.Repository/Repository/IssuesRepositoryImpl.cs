﻿// <copyright file="IssuesRepositoryImpl.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database.Repository
{
    using System.Linq;
    using Net.Easimer.Prog3.Database.Data;

    /// <summary>
    /// Issues repository implementation.
    /// </summary>
    internal class IssuesRepositoryImpl : BaseRepositoryImpl<Issue>, IIssues
    {
        private IIssueTrackerModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="IssuesRepositoryImpl"/> class.
        /// </summary>
        /// <param name="model">Issue tracker model.</param>
        public IssuesRepositoryImpl(IIssueTrackerModel model)
            : base(model.Issues)
        {
            this.model = model;
        }

        /// <inheritdoc/>
        protected override int Flush()
        {
            return this.model.SaveChanges();
        }
    }
}
