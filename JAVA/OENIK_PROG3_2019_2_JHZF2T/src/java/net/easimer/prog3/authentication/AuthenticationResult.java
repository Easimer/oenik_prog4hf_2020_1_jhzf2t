/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.easimer.prog3.authentication;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author danielm
 */
@XmlRootElement(name = "AuthenticationResult")
public class AuthenticationResult {
    public AuthenticationResult() {
        m_success = false;
        m_message = null;
        m_userID = -1;
        m_errorcode = null;
    }
    
    public AuthenticationResult(String msg, String errorCode) {
        m_success = false;
        m_message = msg;
        m_userID = -1;
        m_errorcode = errorCode;
    }
    
    public AuthenticationResult(int userID) {
        m_success = true;
        m_message = null;
        m_userID = userID;
        m_errorcode = null;
    }
    
    @XmlElement(name = "Status")
    private Boolean m_success;
    @XmlElement(name = "Message")
    private String m_message;
    @XmlElement(name = "UserID")
    private int m_userID;
    @XmlElement(name = "ErrorCode")
    private String m_errorcode;
}
