﻿// <copyright file="MiscTests.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Test
{
    using Net.Easimer.Prog3.IssueTracker;
    using NUnit.Framework;

    /// <summary>
    /// Miscellaneous tests.
    /// </summary>
    internal class MiscTests : LogicTestEnvironment
    {
        /// <summary>
        /// Tests IssueTracker::GetUserManagement().
        /// </summary>
        [Test]
        public void TestIssueTrackerGetUserManagement()
        {
            var issueTracker = new IssueTracker(this.DataAccess.Object);
            var userMgmt = issueTracker.GetUserManagement();

            Assert.That(userMgmt, Is.Not.EqualTo(null));
        }

        /// <summary>
        /// Tests IssueTracker::GetProjectManagement().
        /// </summary>
        [Test]
        public void TestIssueTrackerGetProjectManagement()
        {
            var issueTracker = new IssueTracker(this.DataAccess.Object);
            var projMgmt = issueTracker.GetProjectManagement();

            Assert.That(projMgmt, Is.Not.EqualTo(null));
        }

        /// <summary>
        /// Tests IssueTracker::GetIssueManagement().
        /// </summary>
        [Test]
        public void TestIssueTrackerGetIssueManagement()
        {
            var issueTracker = new IssueTracker(this.DataAccess.Object);
            var issueMgmt = issueTracker.GetIssueManagement();

            Assert.That(issueMgmt, Is.Not.EqualTo(null));
        }

        /// <summary>
        /// Tests IssueTracker::GetStatistics().
        /// </summary>
        [Test]
        public void TestIssueTrackerGetStatistics()
        {
            var issueTracker = new IssueTracker(this.DataAccess.Object);
            var stats = issueTracker.GetStatistics();

            Assert.That(stats, Is.Not.EqualTo(null));
        }
    }
}
