﻿// <copyright file="LogicTestEnvironment.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using Net.Easimer.Prog3.Database;
    using Net.Easimer.Prog3.Database.Data;
    using Net.Easimer.Prog3.Database.Repository;
    using Net.Easimer.Prog3.IssueTracker.Impl;
    using NUnit.Framework;

    /// <summary>
    /// Environment for testing logic layer code.
    /// </summary>
    internal class LogicTestEnvironment
    {
        private Mock<IDataAccess> dataAccess;
        private FakeProjectsRepository repoProj;
        private FakeUsersRepository repoUser;
        private FakeIssuesRepository repoIssue;
        private ProjectManager pman;
        private UserManager uman;
        private IssueManager iman;
        private Statistics stats;

        /// <summary>
        /// Gets or sets the Data access mock.
        /// </summary>
        protected Mock<IDataAccess> DataAccess { get => this.dataAccess; set => this.dataAccess = value; }

        /// <summary>
        /// Gets or sets the fake project repository.
        /// </summary>
        protected FakeProjectsRepository RepoProj { get => this.repoProj; set => this.repoProj = value; }

        /// <summary>
        /// Gets or sets the fake user repository.
        /// </summary>
        protected FakeUsersRepository RepoUser { get => this.repoUser; set => this.repoUser = value; }

        /// <summary>
        /// Gets or sets the fake issue repository.
        /// </summary>
        protected FakeIssuesRepository RepoIssue { get => this.repoIssue; set => this.repoIssue = value; }

        /// <summary>
        /// Gets or sets the ProjectManager instance.
        /// </summary>
        protected ProjectManager PMan { get => this.pman; set => this.pman = value; }

        /// <summary>
        /// Gets or sets the UserManager instance.
        /// </summary>
        protected UserManager UMan { get => this.uman; set => this.uman = value; }

        /// <summary>
        /// Gets or sets the IssueManager instance.
        /// </summary>
        protected IssueManager IMan { get => this.iman; set => this.iman = value; }

        /// <summary>
        /// Gets or sets the Statistics instance.
        /// </summary>
        protected Statistics Stats { get => this.stats; set => this.stats = value; }

        /// <summary>
        /// Sets up the Logic test environment.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.RepoProj = new FakeProjectsRepository();
            this.RepoUser = new FakeUsersRepository();
            this.RepoIssue = new FakeIssuesRepository();
            this.DataAccess = new Mock<IDataAccess>();
            this.DataAccess.Setup(x => x.GetProjectsRepository()).Returns(this.RepoProj);
            this.DataAccess.Setup(x => x.GetUsersRepository()).Returns(this.RepoUser);
            this.DataAccess.Setup(x => x.GetIssuesRepository()).Returns(this.RepoIssue);

            this.PMan = new ProjectManager(this.DataAccess.Object);
            this.UMan = new UserManager(this.DataAccess.Object);
            this.IMan = new IssueManager(this.DataAccess.Object);
            this.Stats = new Statistics(this.DataAccess.Object);
        }

        /// <summary>
        /// Generate test project data.
        /// </summary>
        /// <returns>A list containing the test projects.</returns>
        protected List<Project> GenerateProjects()
        {
            List<Project> ret = new List<Project>();
            var proj = new Project();

            proj.ID = 1;
            proj.Name = "Proj1";
            proj.Homepage = "about:blank";
            proj.CreationDate = DateTime.Now;
            proj.Owner = null;
            proj.Parent = null;
            ret.Add(proj);

            proj = new Project();
            proj.ID = 2;
            proj.Name = "Proj2";
            proj.Homepage = "about:blank";
            proj.CreationDate = DateTime.Now;
            proj.Owner = null;
            proj.Parent = 1;
            ret.Add(proj);

            proj = new Project();
            proj.ID = 3;
            proj.Name = "Proj3";
            proj.Homepage = "about:blank";
            proj.CreationDate = DateTime.Now;
            proj.Owner = null;
            proj.Parent = 1;
            ret.Add(proj);

            proj = new Project();
            proj.ID = 4;
            proj.Name = "Proj4";
            proj.Homepage = "about:blank";
            proj.CreationDate = DateTime.Now;
            proj.Owner = null;
            proj.Parent = null;
            ret.Add(proj);

            return ret;
        }

        /// <summary>
        /// Clear all repositories.
        /// </summary>
        protected void ClearRepositories()
        {
            this.RepoProj.Entities.Clear();
            this.RepoUser.Entities.Clear();
            this.RepoIssue.Entities.Clear();
        }

        /// <summary>
        /// Generate test user data.
        /// </summary>
        /// <returns>A list of the generated users.</returns>
        protected List<User> GenerateTestUsers()
        {
            List<User> ret = new List<User>();
            var user = new User();

            user.ID = 1000;
            user.Username = "test0";
            user.FirstName = "I. Teszt";
            user.LastName = "Elek";
            user.EmailAddr = "test0@example.com";
            user.Telephone = "1000";
            ret.Add(user);

            user = new User();
            user.ID = 1001;
            user.Username = "test1";
            user.FirstName = "II. Teszt";
            user.LastName = "Elek";
            user.EmailAddr = "test1@example.com";
            user.Telephone = "1001";
            ret.Add(user);

            user = new User();
            user.ID = 1002;
            user.Username = "test2";
            user.FirstName = "III. Teszt";
            user.LastName = "Elek";
            user.EmailAddr = "test2@example.com";
            user.Telephone = "1002";
            ret.Add(user);

            user = new User();
            user.ID = 1003;
            user.Username = "test3";
            user.FirstName = "IV. Teszt";
            user.LastName = "Elek";
            user.EmailAddr = "test3@example.com";
            user.Telephone = "1003";
            ret.Add(user);

            return ret;
        }

        /// <summary>
        /// Generate test issue data.
        ///
        /// This needs a project with ID#1 to exist.
        /// </summary>
        /// <returns>A list of the generated issues.</returns>
        protected List<Issue> GenerateTestIssues()
        {
            List<Issue> ret = new List<Issue>();
            var issue = new Issue();

            issue.ID = 1;
            issue.Title = "Issue 1";
            issue.Project = 1;
            issue.Solved = false;
            ret.Add(issue);

            issue = new Issue();
            issue.ID = 2;
            issue.Title = "Issue 2";
            issue.Project = 1;
            issue.Solved = true;
            ret.Add(issue);

            return ret;
        }

        /// <summary>
        /// Fake projects repository.
        /// </summary>
        internal class FakeProjectsRepository : FakeListRepository<Project>, IProjects
        {
            /// <inheritdoc/>
            protected override bool CreateCheck(Project value)
            {
                // Check PK constraint
                bool constraintPK = this.Entities.All(x => x.ID != value.ID);
                bool constraintNulls = value.Name != null &&
                    value.Descript != null && value.Homepage != null && value.Lang != null &&
                    value.CreationDate != null;

                return constraintPK && constraintNulls;
            }
        }

        /// <summary>
        /// Fake users repository.
        /// </summary>
        internal class FakeUsersRepository : FakeListRepository<User>, IUsers
        {
            /// <inheritdoc/>
            protected override bool CreateCheck(User value)
            {
                // Check PK constraint
                bool constraintPK = this.Entities.All(ent => ent.ID != value.ID);
                bool constraintNulls = value.Username != null && value.FirstName != null && value.LastName != null;

                return constraintPK && constraintNulls;
            }
        }

        /// <summary>
        /// Fake issues repository.
        /// </summary>
        internal class FakeIssuesRepository : FakeListRepository<Issue>, IIssues
        {
        }
    }
}
