﻿// <copyright file="IssueSelectionWidget.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Net.Easimer.Prog3.IssueTracker;

    /// <summary>
    /// Issue selection widget.
    /// </summary>
    [Selector(typeof(IssueInfo))]
    internal class IssueSelectionWidget : BaseSelectionWidget
    {
        private IIssueManagement issueMgmt;
        private int? projectID;

        /// <summary>
        /// Initializes a new instance of the <see cref="IssueSelectionWidget"/> class.
        /// </summary>
        /// <param name="issueTracker">Issue tracker instance.</param>
        public IssueSelectionWidget(IIssueTracker issueTracker)
            : base(issueTracker)
        {
            this.issueMgmt = issueTracker.GetIssueManagement();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IssueSelectionWidget"/> class.
        /// </summary>
        /// <param name="issueTracker">Issue tracker instance.</param>
        /// <param name="projectID">Project ID to filter by.</param>
        public IssueSelectionWidget(IIssueTracker issueTracker, int projectID)
            : this(issueTracker)
        {
            this.projectID = projectID;
        }

        /// <summary>
        /// Gets the issue manager instance.
        /// </summary>
        protected IIssueManagement IssueMgmt => this.issueMgmt;

        /// <summary>
        /// Gets the project ID.
        /// </summary>
        protected int? ProjectID => this.projectID;

        /// <inheritdoc/>
        protected override IEnumerable<string> GetColumnNames()
        {
            return new string[]
            {
                "#",
                "Title",
                "Solved",
                "Assigned to",
                "Creation date",
                "Tag",
                "Prio",
            };
        }

        /// <inheritdoc/>
        protected override int GetColumnWidth(int colIdx)
        {
            switch (colIdx)
            {
                case 0: return 4;
                case 1: return 72;
                case 2: return 7;
                case 3: return 12;
                case 4: return 16;
                case 5: return 8;
                case 6: return 6;
                default: return 12;
            }
        }

        /// <inheritdoc/>
        protected override IEnumerable<Tuple<object[], object>> GetTableData()
        {
            IQueryable<IssueInfo> queryable = this.issueMgmt.GetAllIssues();

            if (this.projectID.HasValue)
            {
                queryable = queryable.Where(i => i.Project == this.projectID.Value);
            }

            foreach (var row in queryable)
            {
                var ret = new Tuple<object[], object>(
                    new object[]
                    {
                        row.ID,
                        row.Title,
                        row.Solved ? "✓" : "x",
                        row.AssignedTo.HasValue ? row.AssignedToRef.Username : "(none)",
                        row.CreationDate.ToShortDateString(),
                        row.Tag,
                        row.Prio.ToString(),
                    }, row);

                yield return ret;
            }
        }
    }
}
