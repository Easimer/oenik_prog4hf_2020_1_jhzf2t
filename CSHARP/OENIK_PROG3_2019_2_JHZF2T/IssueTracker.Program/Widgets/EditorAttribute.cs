﻿// <copyright file="EditorAttribute.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    /// <summary>
    /// Editor widget for type attribute.
    /// </summary>
    internal class EditorAttribute : Attribute
    {
        private static Dictionary<Type, ConstructorInfo> editorCache = new Dictionary<Type, ConstructorInfo>();
        private Type type;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorAttribute"/> class.
        /// </summary>
        /// <param name="t">Type.</param>
        public EditorAttribute(Type t)
        {
            this.type = t;
        }

        /// <summary>
        /// Gets the editor widget for an object.
        /// </summary>
        /// <param name="edited">Edited object.</param>
        /// <returns>Editor widget instance.</returns>
        public static BaseEditorWidget GetWidgetForType(object edited)
        {
            BaseEditorWidget ret = null;
            var t = edited.GetType();

            ConstructorInfo widgetCtor = null;

            if (editorCache.ContainsKey(t))
            {
                widgetCtor = editorCache[t];
            }
            else
            {
                var assembly = Assembly.GetExecutingAssembly();
                foreach (var type in assembly.GetTypes())
                {
                    var attr = (EditorAttribute)type.GetCustomAttribute(typeof(EditorAttribute));
                    if (attr != null && attr.type.Equals(t))
                    {
                        var types = new Type[]
                        {
                            t,
                        };

                        widgetCtor = type.GetConstructor(types);
                    }
                }
            }

            if (widgetCtor != null)
            {
                var args = new object[]
                {
                    edited,
                };
                ret = widgetCtor.Invoke(args) as BaseEditorWidget;
            }

            return ret;
        }
    }
}
