package net.easimer.prog3.complaints.generator;

import java.util.Random;

/**
 *
 * @author danielm
 */
public class StatementDoesntWork implements BaseStatement {
    private BaseStatement m_feature;
    
    public StatementDoesntWork(Random rnd) {
        m_feature = new StatementFeature(rnd);
    }
    
    @Override
    public String getStatementText() {
        return String.format("Nem mukodik a %s!", m_feature.getStatementText());
    }
}
