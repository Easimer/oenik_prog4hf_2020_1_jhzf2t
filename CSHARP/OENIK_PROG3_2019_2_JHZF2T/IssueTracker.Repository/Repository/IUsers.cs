﻿// <copyright file="IUsers.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database.Repository
{
    using Net.Easimer.Prog3.Database;
    using Net.Easimer.Prog3.Database.Data;

    /// <summary>
    /// Users repository.
    /// </summary>
    public interface IUsers : IRepository<User>
    {
    }
}
