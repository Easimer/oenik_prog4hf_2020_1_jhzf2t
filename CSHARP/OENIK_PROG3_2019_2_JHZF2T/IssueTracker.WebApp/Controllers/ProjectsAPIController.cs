﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Net.Easimer.Prog3.Database;
using Net.Easimer.Prog3.IssueTracker.WebApp.Models;

namespace Net.Easimer.Prog3.IssueTracker.WebApp.Controllers
{
    public class ProjectsAPIController : ApiController
    {
        private IIssueTracker issueTracker;
        private IProjectManagement projMgmt;
        private IUserManagement userMgmt;
        private IMapper mapper;

        public class Response 
        {
            public bool Result { get; set; }
        }

        public ProjectsAPIController()
        {
            IDataAccess datacc = new DataAccessImpl();
            issueTracker = new IssueTracker(datacc);
            projMgmt = issueTracker.GetProjectManagement();
            userMgmt = issueTracker.GetUserManagement();

            mapper = MapperFactory.CreateMapper();
        }

        // GET api/ProjectsAPI/all
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.ProjectModel> GetAll()
        {
            return mapper.Map<IEnumerable<ProjectInfo>, IEnumerable<ProjectModel>>(
                projMgmt.GetAllProjects()
            );
        }

        // GET api/ProjectsAPI/del/42
        [ActionName("del")]
        [HttpGet]
        public Response DelOne(int id)
        {
            return new Response { Result = projMgmt.DeleteByID(id) };
        }

        // POST api/ProjectsAPI/add
        [ActionName("add")]
        [HttpPost]
        public Response AddOne(ProjectModel proj)
        {
            var res = projMgmt.CreateProject(proj.Id, proj.Name, proj.Homepage, proj.Owner, proj.Parent, DateTime.Now);
            return new Response { Result = res != null };
        }

        // POST api/ProjectsAPI/edit
        [ActionName("edit")]
        [HttpPost]
        public Response EditOne(ProjectModel proj)
        {
            var oldProj = projMgmt.GetByID(proj.Id);
            oldProj.Name = proj.Name;
            oldProj.Homepage = proj.Homepage;
            oldProj.Owner = proj.Owner;
            projMgmt.Modify(proj.Id, oldProj);

            return new Response { Result = true };
        }
    }
}
