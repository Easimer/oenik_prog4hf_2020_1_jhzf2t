﻿// <copyright file="DateTimeEditor.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System;

    /// <summary>
    /// DateTime editor.
    /// </summary>
    [Editor(typeof(DateTime))]
    internal class DateTimeEditor : BaseEditorWidget
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DateTimeEditor"/> class.
        /// </summary>
        /// <param name="value">Value.</param>
        public DateTimeEditor(DateTime value)
            : base(value)
        {
        }

        /// <inheritdoc/>
        public override bool Update()
        {
            var dt = (DateTime)this.Edited;
            int year = dt.Year;
            int month = dt.Month;
            int day = dt.Day;

            ImTUI.Input("Year", ref year);
            ImTUI.Input("Month", ref month);
            ImTUI.Input("Day", ref day);

            if (ImTUI.Button("Set to today"))
            {
                this.Edited = DateTime.Now;
            }
            else
            {
                try
                {
                    this.Edited = new DateTime(year, month, day);
                }
                catch (ArgumentOutOfRangeException)
                {
                    this.Edited = DateTime.Now;
                }
            }

            return true;
        }
    }
}
