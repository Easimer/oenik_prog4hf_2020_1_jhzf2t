﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueTracker.Wpf
{
    public interface IAPIComms
    {
        IEnumerable<ProjectViewModel> GetAllProjects();

        void Delete(ProjectViewModel project);

        bool Edit(ProjectViewModel project, bool isEditing);
    }
}
