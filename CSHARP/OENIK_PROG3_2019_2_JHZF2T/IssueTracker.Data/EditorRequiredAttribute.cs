﻿// <copyright file="EditorRequiredAttribute.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database
{
    using System;

    /// <summary>
    /// Marks an entity field as required.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = true)]
    public class EditorRequiredAttribute : Attribute
    {
    }
}
