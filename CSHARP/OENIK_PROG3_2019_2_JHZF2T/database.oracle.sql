DROP TABLE ISSUES;
DROP TABLE PROJECTS;
DROP TABLE USERS;

CREATE TABLE USERS (
    ID NUMBER NOT NULL,
    USERNAME VARCHAR2(64) NOT NULL,
    FIRSTNAME VARCHAR2(128) NOT NULL,
    LASTNAME VARCHAR2(64) NOT NULL,
    EMAILADDR VARCHAR2(256) NOT NULL,
    TELEPHONE VARCHAR2(32),
    
    CONSTRAINT USERS_PK PRIMARY KEY(ID)
);

CREATE TABLE PROJECTS (
    ID NUMBER NOT NULL,
    OWNER NUMBER DEFAULT NULL,
    NAME VARCHAR2(64) NOT NULL,
    HOMEPAGE VARCHAR2(256) DEFAULT NULL,
    CREATIONDATE DATE NOT NULL,
    PARENT NUMBER DEFAULT NULL,
    
    CONSTRAINT PROJECTS_PK PRIMARY KEY(ID),
    CONSTRAINT PROJECTS_OWNER_USERS_FK FOREIGN KEY(OWNER) REFERENCES USERS(ID) ON DELETE SET NULL,
    CONSTRAINT PROJECTS_PARENT_PROJECTS_FK FOREIGN KEY(PARENT) REFERENCES PROJECTS(ID) ON DELETE SET NULL
);

CREATE TABLE ISSUES (
    ID NUMBER NOT NULL,
    PROJECT NUMBER NOT NULL,
    TITLE VARCHAR2(512) NOT NULL,
    ASSIGNEDTO NUMBER DEFAULT NULL,
    SOLVED NUMBER(1) NOT NULL,
    
    CONSTRAINT ISSUES_PK PRIMARY KEY(ID),
    CONSTRAINT ISSUES_ASSIGNEDTO_USERS_FK FOREIGN KEY(ASSIGNEDTO) REFERENCES USERS(ID) ON DELETE SET NULL,
    CONSTRAINT ISSUES_PROJECT_PROJECTS_FK FOREIGN KEY(PROJECT) REFERENCES PROJECTS(ID) ON DELETE CASCADE,
    
    CONSTRAINT ISSUES_SOLVED_BOOLEAN CHECK (SOLVED IN (0, 1))
);

INSERT ALL
    INTO USERS VALUES(1, 'danielm', 'Daniel', 'Meszaros', 'danielm@example.com', '12301')
    INTO USERS VALUES(2, 'alexanderm', 'Alexander', 'Morris', 'alexanderm@example.com', '12302')
    INTO USERS VALUES(3, 'tinam', 'Tina', 'Myers', 'tinam@example.com', '12303')
    INTO USERS VALUES(4, 'saulb', 'Saul', 'Benjamin', 'saulb@example.com', '')
    INTO USERS VALUES(5, 'warrenm', 'Warren', 'Mills', 'warrenm@example.com', '12305')
    INTO USERS VALUES(6, 'zahrav', 'Zahra', 'Vincent', 'zahrav@example.com', '12306')
    INTO USERS VALUES(7, 'georger', 'George', 'Raymond', 'georger@example.com', '')
    INTO USERS VALUES(8, 'adilg', 'Adil', 'Garrison', 'adilg@example.com', '12308')
SELECT * FROM DUAL;

COMMIT WORK;

INSERT ALL
    INTO PROJECTS VALUES(1, NULL, 'IssueTracker', NULL, CURRENT_DATE, NULL)
    INTO PROJECTS VALUES(2, 1, 'IssueTracker Repository', NULL, CURRENT_DATE, 1)
    INTO PROJECTS VALUES(3, 1, 'IssueTracker Logic', NULL, CURRENT_DATE, 1)
    INTO PROJECTS VALUES(4, 1, 'IssueTracker UI', NULL, CURRENT_DATE, 1)
    INTO PROJECTS VALUES(5, 1, 'IssueTracker Testing', NULL, CURRENT_DATE, 1)
    INTO PROJECTS VALUES(6, 1, 'IssueTracker Web Backend', NULL, CURRENT_DATE, 1)
SELECT * FROM DUAL;

COMMIT WORK;

INSERT ALL
    INTO ISSUES VALUES(1, 4, 'Implement password input field', 1, 1)
    INTO ISSUES VALUES(2, 3, 'Generate statistics about issues per project and per users.', 1, 0)
    INTO ISSUES VALUES(3, 4, 'Display the statistics mentioned in #3', 1, 0)
    INTO ISSUES VALUES(4, 3, 'Implement user statistics.', 1, 0)
    INTO ISSUES VALUES(5, 5, 'Test user authentication errors.', 2, 1)
    INTO ISSUES VALUES(6, 4, 'DIsplay success messagebox when creating a new issue.', 3, 1)
    INTO ISSUES VALUES(7, 3, 'Implement retrieval of user complaints from backend.', 3, 0)
    INTO ISSUES VALUES(8, 4, 'Fix keypresses getting stuck in Backbuffer queue.', 1, 1)
    INTO ISSUES VALUES(9, 5, 'Create a faulty mode in FakeListRepository to simulate database errors.', NULL, 0)
    INTO ISSUES VALUES(10, 5, 'Check consistency in FakeListRepository.', 3, 0)
    INTO ISSUES VALUES(11, 3, 'Make web auth testable.', 6, 1)
SELECT * FROM DUAL;

COMMIT WORK;

SELECT * FROM USERS;
SELECT * FROM PROJECTS;
SELECT * FROM ISSUES;

PROMPT ============= SQ =============
SELECT * FROM PROJECTS ORDER BY NAME ASC;

SELECT USERNAME, FIRSTNAME || ' ' || LASTNAME FROM USERS WHERE LOWER(USERNAME) LIKE '%l%' ORDER BY USERNAME;

