$env:Path += ";C:\Users\easimer\AppData\Local\Apps\OpenCover\";
$env:Path += ";C:\Program Files (x86)\NUnit.org\nunit-console";

OpenCover.Console -register:user -target:nunit3-console.exe -targetargs:".\IssueTracker.Logic.Tests\bin\Debug\IssueTracker.Logic.Tests.dll" -output:TestResult.xml -filter:"+[*.Logic]*"