package net.easimer.prog3.complaints.generator;

import java.util.Random;

/**
 *
 * @author danielm
 */
public class StatementFeature implements BaseStatement {
    private String m_feature;
    
    private static String[] features = new String[] {
      "program", "hozzáadás", "törlés", "keresés", "lekérdezés"
    };
    
    public StatementFeature(Random rnd) {
        m_feature = features[rnd.nextInt(features.length)];
    }
    
    @Override
    public String getStatementText() {
        return m_feature;
    }
}
