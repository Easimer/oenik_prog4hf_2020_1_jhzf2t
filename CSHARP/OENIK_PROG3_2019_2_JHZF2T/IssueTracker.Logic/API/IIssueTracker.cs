﻿// <copyright file="IIssueTracker.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker
{
    using System;

    /// <summary>
    /// Issue tracker interface.
    /// </summary>
    public interface IIssueTracker
    {
        /// <summary>
        /// Connects to a web backend.
        /// </summary>
        /// <param name="baseURL">Base URL of the backend.</param>
        /// <returns>A IWebProg3 instance.</returns>
        IWebProg3 ConnectToBackend(string baseURL);

        /// <summary>
        /// Create an user management interface.
        /// </summary>
        /// <returns>An IUserManagement instance.</returns>
        IUserManagement GetUserManagement();

        /// <summary>
        /// Create a project management interface.
        /// </summary>
        /// <returns>An IProjectManagement instance.</returns>
        IProjectManagement GetProjectManagement();

        /// <summary>
        /// Create an issue management interface.
        /// </summary>
        /// <returns>An IIssueManagement instance.</returns>
        IIssueManagement GetIssueManagement();

        /// <summary>
        /// Create a statistics interface.
        /// </summary>
        /// <returns>An IStatistics instance.</returns>
        IStatistics GetStatistics();
    }
}
