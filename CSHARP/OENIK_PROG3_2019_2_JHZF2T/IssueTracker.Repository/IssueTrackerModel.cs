﻿// <copyright file="IssueTrackerModel.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database.Data
{
    using System.Data.Entity;

    /// <summary>
    /// Issue tracker database model.
    /// </summary>
    public class IssueTrackerModel : DbContext, IIssueTrackerModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IssueTrackerModel"/> class.
        /// </summary>
        public IssueTrackerModel()
            : base("name=IssueTrackerModel")
        {
        }

        /// <inheritdoc/>
        public virtual DbSet<User> Users { get; set; }

        /// <inheritdoc/>
        public virtual DbSet<Project> Projects { get; set; }

        /// <inheritdoc/>
        public virtual DbSet<Issue> Issues { get; set; }

        /// <summary>
        /// OnModelCreating callback.
        /// </summary>
        /// <param name="modelBuilder">Model builder handle.</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("feleves");

            this.SetupKeys(modelBuilder);
            this.SetupUsers(modelBuilder);
            this.SetupProjects(modelBuilder);
            this.SetupIssues(modelBuilder);
        }

        private void SetupKeys(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>().HasKey(x => x.ID);
            modelBuilder.Entity<User>().HasKey(x => x.ID);
            modelBuilder.Entity<Issue>().HasKey(x => x.ID);
        }

        private void SetupUsers(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().Property(p => p.Username).IsRequired().HasMaxLength(64);
            modelBuilder.Entity<User>().Property(u => u.FirstName).IsRequired().HasMaxLength(128);
            modelBuilder.Entity<User>().Property(u => u.LastName).IsRequired().HasMaxLength(64);
            modelBuilder.Entity<User>().Property(u => u.EmailAddr).IsRequired().HasMaxLength(256);
            modelBuilder.Entity<User>().Property(u => u.Telephone).IsRequired().HasMaxLength(32);
        }

        private void SetupProjects(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>().Property(p => p.Name).IsRequired().HasMaxLength(64);
            modelBuilder.Entity<Project>().Property(p => p.Homepage).IsOptional().HasMaxLength(128);
            modelBuilder.Entity<Project>()
                .HasOptional(p => p.OwnerRef)
                .WithMany()
                .HasForeignKey(p => p.Owner);

            modelBuilder.Entity<Project>()
                .HasOptional(p => p.ParentRef)
                .WithMany()
                .HasForeignKey(p => p.Parent);
        }

        private void SetupIssues(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Issue>()
                .Property(i => i.Title).IsRequired().HasMaxLength(512);
            modelBuilder.Entity<Issue>()
                .HasRequired(p => p.ProjectRef)
                .WithMany()
                .HasForeignKey(p => p.Project);

            modelBuilder.Entity<Issue>()
                .HasOptional(p => p.AssignedToRef)
                .WithMany()
                .HasForeignKey(p => p.AssignedTo);
        }
    }
}
