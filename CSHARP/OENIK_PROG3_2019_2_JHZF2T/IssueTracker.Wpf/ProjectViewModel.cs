﻿namespace IssueTracker.Wpf
{
    using System;
    using GalaSoft.MvvmLight;

    public class ProjectViewModel : ObservableObject
    {
        private int id;
        private string name;
        private int? owner;
        private string ownerName;
        private string homepage;
        private DateTime creationDate;
        private int? parent;
        private string parentProjectName;

        public int Id
        {
            get => this.id;
            set => Set(ref this.id, value);
        }

        public string Name
        {
            get => this.name;
            set => Set(ref this.name, value);
        }

        public int? Owner
        {
            get => this.owner;
            set => Set(ref this.owner, value);
        }

        public string OwnerName
        {
            get => this.ownerName;
            set => Set(ref this.ownerName, value);
        }

        public string Homepage
        {
            get => this.homepage;
            set => Set(ref this.homepage, value);
        }

        public DateTime CreationDate
        {
            get => this.creationDate;
            set => Set(ref this.creationDate, value);
        }

        public int? Parent
        {
            get => this.parent;
            set => Set(ref this.parent, value);
        }

        public string ParentProjectName
        {
            get => this.parentProjectName;
            set => Set(ref this.parentProjectName, value);
        }

        public void CopyFrom(ProjectViewModel other)
        {
            if (other != null)
            {
                this.Id = other.Id;
                this.Name = other.Name;
                this.Owner = other.Owner;
                this.OwnerName = other.OwnerName;
                this.Homepage = other.Homepage;
                this.CreationDate = other.CreationDate;
                this.Parent = other.Parent;
                this.ParentProjectName = other.ParentProjectName;
            }
        }
    }
}
