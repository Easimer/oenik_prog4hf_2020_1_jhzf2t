﻿// <copyright file="WebProg3Impl.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker.Impl
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Xml;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    /// <summary>
    /// IWebProg3 implementation.
    /// </summary>
    public class WebProg3Impl : IWebProg3, IDisposable
    {
        private HttpClient httpClient;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebProg3Impl"/> class.
        /// </summary>
        /// <param name="baseURL">Base URL of the backend endpoints.</param>
        /// <exception cref="UriFormatException">Thrown when the URL is invalid.</exception>
        /// <exception cref="ArgumentNullException">Thrown when the URL is null.</exception>
        public WebProg3Impl(string baseURL)
        {
            Uri uri;
            this.httpClient = new HttpClient();

            uri = new Uri(baseURL);

            this.httpClient.BaseAddress = uri;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WebProg3Impl"/> class.
        /// </summary>
        /// <param name="client">A HttpClient instance.</param>
        public WebProg3Impl(HttpClient client)
        {
            this.httpClient = client;
        }

        /// <inheritdoc/>
        public int Authenticate(string username, string password)
        {
            var task = this.AuthAsync(username, password);

            try
            {
                task.Wait();
                return task.Result;
            }
            catch (AggregateException ex)
            {
                // NOTE(danielm): rethrow the first auth ex we find in there
                for (int i = 0; i < ex.InnerExceptions.Count; i++)
                {
                    if (ex.InnerExceptions[i] is AuthenticationException)
                    {
                        throw ex.InnerExceptions[i];
                    }
                }

                // NOTE(danielm): if no auth ex was recv'd then assume network errors and throw one
                throw new AuthenticationException("Couldn't connect to the login server! (use username 'admin' to bypass");
            }
        }

        /// <inheritdoc/>
        public IEnumerable<Complaint> GetComplaints()
        {
            using (var req = this.GetComplaintsDoRequest())
            {
                var stream = req.Content.ReadAsStreamAsync().Result;
                IEnumerable<Complaint> complaints;

                complaints = this.FromStreamComplaints(stream);

                if (complaints != null)
                {
                    foreach (var complaint in complaints)
                    {
                        yield return complaint;
                    }
                }
            }
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            this.httpClient?.Dispose();
        }

        private async Task<int> AuthAsync(string username, string password)
        {
            int ret = -1;
            var dict = new Dictionary<string, string>();
            dict.Add("username", username);
            dict.Add("password", password);
            HttpResponseMessage res;

            if (username == "admin")
            {
                return 1;
            }

            try
            {
                res = await this.httpClient.PostAsync("Authenticate/v1", new FormUrlEncodedContent(dict));
            }
            catch (Exception ex)
            {
                throw new Exception($"Internal error: '{ex.Message}'", ex);
            }

            if (res.StatusCode == HttpStatusCode.OK)
            {
                var stream = await res.Content.ReadAsStreamAsync();
                var authres = this.DeserializeAuthResponse(stream);

                if (authres.Status)
                {
                    ret = authres.UserID;
                }
                else
                {
                    throw new AuthenticationException(authres.Message);
                }
            }
            else
            {
                throw new Exception($"Couldn't communicate with the authentication server! (use username 'admin' to bypass)");
            }

            return ret;
        }

        /// <summary>
        /// Converts a stream of XML content to Complaints.
        /// </summary>
        /// <param name="stream">A valid stream.</param>
        /// <returns>An array of Complaints.</returns>
        /// <exception cref="InvalidDataException">Thrown when the stream does not contain valid XML data.</exception>
        private IEnumerable<Complaint> FromStreamComplaints(Stream stream)
        {
            ComplaintsResult cres = null;
            var serializer = new XmlSerializer(typeof(ComplaintsResult));
            try
            {
                cres = serializer.Deserialize(stream) as ComplaintsResult;
            }
            catch (InvalidOperationException)
            {
                throw new Exception("Couldn't communicate with the servers!");
            }

            if (cres != null && cres.Complaints != null)
            {
                foreach (var complaint in cres.Complaints)
                {
                    yield return complaint;
                }
            }
        }

        /// <summary>
        /// Performs the HTTP request to get the list of complaints.
        /// </summary>
        /// <returns>HttpResponseMessage of the request.</returns>
        private HttpResponseMessage GetComplaintsDoRequest()
        {
            HttpResponseMessage res;

            try
            {
                res = this.httpClient.GetAsync("GetComplaints/v1").Result;
            }
            catch (Exception ex)
            {
                throw new Exception($"Internal error: '{ex.Message}'", ex);
            }

            if (res.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception($"Couldn't communicate with the authentication server!");
            }

            return res;
        }

        private AuthenticationResult DeserializeAuthResponse(Stream s)
        {
            var ser = XmlSerializer.FromTypes(new Type[] { typeof(AuthenticationResult) })[0];
            return (AuthenticationResult)ser.Deserialize(s);
        }

        /// <summary>
        /// Authentication result.
        /// </summary>
        public class AuthenticationResult
        {
            [XmlElement]
            private bool status;
            [XmlElement(IsNullable = true)]
            private string message;
            [XmlElement]
            private int userID;

            /// <summary>
            /// Gets or sets the error message (if any).
            /// </summary>
            public string Message { get => this.message; set => this.message = value; }

            /// <summary>
            /// Gets or sets the user ID.
            /// </summary>
            public int UserID { get => this.userID; set => this.userID = value; }

            /// <summary>
            /// Gets or sets a value indicating whether the authentication succeeded.
            /// </summary>
            public bool Status { get => this.status; set => this.status = value; }
        }

        /// <summary>
        /// Complaints result.
        /// </summary>
        [XmlTypeAttribute(AnonymousType = true)]
        [XmlRootAttribute(Namespace = "", IsNullable = false)]
        public class ComplaintsResult
        {
            private Complaint[] complaints;

            /// <summary>
            /// Gets or sets the list of complaints.
            /// </summary>
            [XmlElementAttribute("Complaint", Form = XmlSchemaForm.Unqualified)]
            public Complaint[] Complaints { get => this.complaints; set => this.complaints = value; }
        }
    }
}
