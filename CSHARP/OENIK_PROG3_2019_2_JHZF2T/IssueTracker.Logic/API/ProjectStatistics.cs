﻿// <copyright file="ProjectStatistics.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker
{
    /// <summary>
    /// Project statistics.
    /// </summary>
    public struct ProjectStatistics
    {
        /// <summary>
        /// Identifier of the project.
        /// </summary>
        public int ProjectID;

        /// <summary>
        /// Name of the project.
        /// </summary>
        public string ProjectName;

        /// <summary>
        /// Number of total issues.
        /// </summary>
        public int TotalIssues;

        /// <summary>
        /// Number of solved issues.
        /// </summary>
        public int SolvedIssues;

        /// <summary>
        /// Number of unsolved issues.
        /// </summary>
        public int UnsolvedIssues;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectStatistics"/> struct.
        /// </summary>
        /// <param name="id">Project identifier.</param>
        /// <param name="name">Project name.</param>
        /// <param name="totalIssues">Number of total issues.</param>
        /// <param name="unsolvedIssues">Number of unsolved issues.</param>
        /// <param name="solvedIssues">Number of solved issues.</param>
        public ProjectStatistics(int id, string name, int totalIssues, int unsolvedIssues, int solvedIssues)
        {
            this.ProjectID = id;
            this.ProjectName = name;
            this.TotalIssues = totalIssues;
            this.UnsolvedIssues = unsolvedIssues;
            this.SolvedIssues = solvedIssues;
        }
    }
}
