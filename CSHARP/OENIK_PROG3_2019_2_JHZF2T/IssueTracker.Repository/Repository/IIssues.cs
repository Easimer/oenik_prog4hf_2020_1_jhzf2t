﻿// <copyright file="IIssues.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database.Repository
{
    using Net.Easimer.Prog3.Database.Data;

    /// <summary>
    /// Issues repository interface.
    /// </summary>
    public interface IIssues : IRepository<Issue>
    {
    }
}
