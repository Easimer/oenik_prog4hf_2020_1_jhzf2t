var searchData=
[
  ['savechanges_111',['SaveChanges',['../interface_net_1_1_easimer_1_1_prog3_1_1_database_1_1_data_1_1_i_issue_tracker_model.html#a298c2aa0750558661ce75da5b9e423e3',1,'Net::Easimer::Prog3::Database::Data::IIssueTrackerModel']]],
  ['sethomepage_112',['SetHomepage',['../interface_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_i_project_management.html#a36dde668b5ba1354d62017c7c061b136',1,'Net::Easimer::Prog3::IssueTracker::IProjectManagement']]],
  ['setowner_113',['SetOwner',['../interface_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_i_project_management.html#a7998779e3803bd7d00c8296840007620',1,'Net::Easimer::Prog3::IssueTracker::IProjectManagement']]],
  ['solved_114',['Solved',['../class_net_1_1_easimer_1_1_prog3_1_1_database_1_1_data_1_1_issue.html#ae11726c1384d864e4632ae18e110d4d9',1,'Net::Easimer::Prog3::Database::Data::Issue']]],
  ['solvedissues_115',['SolvedIssues',['../struct_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_project_statistics.html#adbe4451e4fc047fb3568c1d82c48a178',1,'Net.Easimer.Prog3.IssueTracker.ProjectStatistics.SolvedIssues()'],['../struct_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_user_statistics.html#a27948d85da2eb8b474ab132f3ba7797f',1,'Net.Easimer.Prog3.IssueTracker.UserStatistics.SolvedIssues()']]],
  ['status_116',['Status',['../class_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_impl_1_1_web_prog3_impl_1_1_authentication_result.html#aac326b6ed212a7995be614f9ec556dfe',1,'Net::Easimer::Prog3::IssueTracker::Impl::WebProg3Impl::AuthenticationResult']]]
];
