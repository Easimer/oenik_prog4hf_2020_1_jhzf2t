﻿// <copyright file="ProjectInfo.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker
{
    using System;
    using Net.Easimer.Prog3.Database;
    using Net.Easimer.Prog3.Database.Data;
    using Net.Easimer.Prog3.Database.Repository;

    /// <summary>
    /// Project information.
    /// </summary>
    public class ProjectInfo : Project
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectInfo"/> class.
        /// </summary>
        public ProjectInfo()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectInfo"/> class.
        /// </summary>
        /// <param name="projects">Projects repository.</param>
        /// <param name="users">Users repository.</param>
        /// <param name="prj">Project entity.</param>
        public ProjectInfo(IProjects projects, IUsers users, Project prj)
            : base(prj)
        {
            UserInfo owner_info = null;
            ProjectInfo parent_info = null;

            if (prj.Owner.HasValue)
            {
                owner_info = new UserInfo(users.GetById(prj.Owner.Value));
            }

            if (prj.Parent.HasValue)
            {
                parent_info = new ProjectInfo(projects, users, projects.GetById(prj.Parent.Value));
            }

            this.OwnerRef = owner_info;
            this.ParentRef = parent_info;
        }

        /// <summary>
        /// Gets or sets the reference to the owner.
        /// </summary>
        [EditorDescription("Owner")]
        public new UserInfo OwnerRef { get; set; }

        /// <summary>
        /// Gets or sets the reference to the parent.
        /// </summary>
        [EditorDescription("Parent project")]
        public new ProjectInfo ParentRef { get; set; }
    }
}
