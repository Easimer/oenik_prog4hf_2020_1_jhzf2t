﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IssueTracker.Wpf
{
    internal class MainLogic
    {
        private IAPIComms comm;

        public MainLogic()
        {
            this.comm = new APIComms("http://localhost:65413/");
        }

        public void Edit(ProjectViewModel project, Func<ProjectViewModel, bool> editor)
        {
            var clone = new ProjectViewModel();
            if (project != null)
            {
                clone.CopyFrom(project);
            }

            var succ = editor?.Invoke(clone);
            if (succ == true)
            {
                if (project != null)
                {
                    succ = this.comm.Edit(clone, true);
                }
                else
                {
                    succ = this.comm.Edit(clone, false);
                }
            }
        }
        public void Delete(ProjectViewModel project)
        {
            this.comm.Delete(project);
        }

        public IEnumerable<ProjectViewModel> GetAllProjects()
        {
            return this.comm.GetAllProjects();
        }
    }
}
