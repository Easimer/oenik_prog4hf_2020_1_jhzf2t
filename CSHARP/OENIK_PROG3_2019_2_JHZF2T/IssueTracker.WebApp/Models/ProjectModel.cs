﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Net.Easimer.Prog3.IssueTracker.WebApp.Models
{
    // Form Model
    public class ProjectModel
    {
        [Display(Name = "ID")]
        [Required]
        public int Id { get; set; }

        [Display]
        [Required]
        public string Name { get; set; }

        [Required]
        public int? Owner { get; set; }

        public string OwnerName { get; set; }

        public string Homepage { get; set; }

        [Required]
        [Display(Name = "Creation date")]
        public DateTime CreationDate { get; set; }

        [Display(Name = "Parent Project ID")]
        public int? Parent { get; set; }

        [Display(Name = "Parent Project")]
        public string ParentProjectName { get; set; }
    }
}