﻿// <copyright file="IUserManagement.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// User management interface.
    /// </summary>
    public interface IUserManagement
    {
        /// <summary>
        /// Register a new user.
        /// </summary>
        /// <param name="id">User ID.</param>
        /// <param name="username">Username.</param>
        /// <param name="firstName">User's first name.</param>
        /// <param name="lastName">User's last name.</param>
        /// <param name="email">User's email address.</param>
        /// <param name="telephone">User's telephone number.</param>
        /// <exception cref="UserManagementException">Thrown on error.</exception>
        /// <returns>The created user.</returns>
        UserInfo RegisterNewUser(int id, string username, string firstName, string lastName, string email = null, string telephone = null);

        /// <summary>
        /// Get list of all users.
        /// </summary>
        /// <exception cref="UserManagementException">Thrown on error.</exception>
        /// <returns>List of all users.</returns>
        IEnumerable<UserInfo> GetAllUsers();

        /// <summary>
        /// Get user by ID.
        /// </summary>
        /// <param name="id">User ID.</param>
        /// <exception cref="UserManagementException">Thrown on error.</exception>
        /// <returns>User info or null.</returns>
        UserInfo GetUserByID(int id);

        /// <summary>
        /// Delete a user by ID. Issues assigned to this user will be orphaned. Users who own any projects cannot be deleted.
        /// </summary>
        /// <param name="id">User ID.</param>
        /// <exception cref="UserManagementException">Thrown on error.</exception>
        /// <returns>Whether the deletion succeeded.</returns>
        bool DeleteUser(int id);

        /// <summary>
        /// Rename a user.
        /// </summary>
        /// <param name="subject">The user who is about to be renamed.</param>
        /// <param name="firstName">New first name or null to keep the old name.</param>
        /// <param name="lastName">New last name or null to keep the old name.</param>
        /// <returns>The resulting UserInfo.</returns>
        UserInfo RenameUser(UserInfo subject, string firstName, string lastName);

        /// <summary>
        /// Rename a user by ID.
        /// </summary>
        /// <param name="userID">The ID of the user who's about to be renamed.</param>
        /// <param name="firstName">New first name or null to keep the old name.</param>
        /// <param name="lastName">New last name or null to keep the old name.</param>
        /// <returns>The resulting UserInfo.</returns>
        UserInfo RenameUser(int userID, string firstName, string lastName);
    }
}
