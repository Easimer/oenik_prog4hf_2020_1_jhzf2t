﻿// <copyright file="UserInfo.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker
{
    using System.Text;
    using Net.Easimer.Prog3.Database.Data;

    /// <summary>
    /// User information.
    /// </summary>
    public class UserInfo : User
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserInfo"/> class.
        /// </summary>
        public UserInfo()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserInfo"/> class.
        /// </summary>
        /// <param name="ent">User entity.</param>
        public UserInfo(User ent)
            : base(ent)
        {
        }
    }
}
