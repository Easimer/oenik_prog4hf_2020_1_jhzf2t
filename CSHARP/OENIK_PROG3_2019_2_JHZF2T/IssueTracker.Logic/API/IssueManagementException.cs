﻿// <copyright file="IssueManagementException.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker
{
    using Net.Easimer.Prog3.Database.Data;

    /// <summary>
    /// Issue management exception.
    /// </summary>
    public class IssueManagementException : BaseManagementException<IssueInfo>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IssueManagementException"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <param name="issue">The issue related to the incident.</param>
        public IssueManagementException(string message = "", IssueInfo issue = null)
            : base(message, issue)
        {
        }
    }
}
