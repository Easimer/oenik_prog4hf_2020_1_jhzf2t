﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace IssueTracker.Wpf
{
    class MainViewModel : ViewModelBase
    {
        private MainLogic logic;
        private ObservableCollection<ProjectViewModel> projects;
        private ProjectViewModel selected;

        public ObservableCollection<ProjectViewModel> Projects
        {
            get => this.projects;
            set => Set(ref this.projects, value);
        }

        public ProjectViewModel Selected
        {
            get => this.selected;
            set => Set(ref this.selected, value);
        }

        public ICommand Add { get; private set; }
        public ICommand Edit { get; private set; }
        public ICommand Delete { get; private set; }
        public ICommand Load { get; private set; }

        public Func<ProjectViewModel, bool> EditorCallback { get; set; }

        public MainViewModel()
        {
            this.logic = new MainLogic();

            this.Delete = new RelayCommand(() => logic.Delete(this.selected));
            this.Add = new RelayCommand(() => logic.Edit(null, this.EditorCallback));
            this.Edit = new RelayCommand(() => logic.Edit(this.selected, this.EditorCallback));
            this.Load = new RelayCommand(
                () =>
                    this.Projects = new ObservableCollection<ProjectViewModel>(logic.GetAllProjects())
            );
        }
    }
}
