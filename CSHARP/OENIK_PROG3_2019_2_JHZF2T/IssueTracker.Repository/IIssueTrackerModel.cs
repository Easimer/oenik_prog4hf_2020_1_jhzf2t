﻿// <copyright file="IIssueTrackerModel.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database.Data
{
    using System.Data.Entity;

    /// <summary>
    /// Issue tracker model interface.
    /// </summary>
    public interface IIssueTrackerModel
    {
        /// <summary>
        /// Gets or sets the table of users.
        /// </summary>
        DbSet<User> Users { get; set; }

        /// <summary>
        /// Gets or sets the table of users.
        /// </summary>
        DbSet<Project> Projects { get; set; }

        /// <summary>
        /// Gets or sets the table of issues.
        /// </summary>
        DbSet<Issue> Issues { get; set; }

        /// <summary>
        /// Save changes.
        /// </summary>
        /// <returns>Integer value.</returns>
        int SaveChanges();
    }
}
