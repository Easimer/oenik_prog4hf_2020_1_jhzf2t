﻿// <copyright file="Statistics.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Net.Easimer.Prog3.Database;
    using Net.Easimer.Prog3.IssueTracker;

    /// <summary>
    /// Implements the IStatistics interface.
    /// </summary>
    internal class Statistics : IStatistics
    {
        private IDataAccess dataAccess;

        /// <summary>
        /// Initializes a new instance of the <see cref="Statistics"/> class.
        /// </summary>
        /// <param name="dataAccess">An IDataAccess instance.</param>
        public Statistics(IDataAccess dataAccess)
        {
            this.dataAccess = dataAccess;
        }

        /// <inheritdoc/>
        public IEnumerable<ProjectStatistics> GetProjectStatistics()
        {
            var issueRepo = this.dataAccess.GetIssuesRepository();
            var projRepo = this.dataAccess.GetProjectsRepository();

            var res = from issue in issueRepo.ReadAll()
                      group issue by issue.Project into groups
                      select new
                      {
                          ID = groups.Key,
                          AllIssues = groups.Count(),
                          UnsolvedIssues = groups.Where(g => !g.Solved).Count(),
                          SolvedIssues = groups.Where(g => g.Solved).Count(),
                      };

            foreach (var result in res)
            {
                yield return new ProjectStatistics(result.ID, projRepo.GetById(result.ID).Name, result.AllIssues, result.UnsolvedIssues, result.SolvedIssues);
            }
        }

        /// <inheritdoc/>
        public ProjectStatistics GetProjectStatistics(int projectID)
        {
            var projRepo = this.dataAccess.GetProjectsRepository();
            var issueRepo = this.dataAccess.GetIssuesRepository();
            try
            {
                var proj = projRepo.GetById(projectID);

                int id = projectID;
                string name = proj.Name;

                var res = from issue in issueRepo.ReadAll()
                          where issue.Project == projectID
                          group issue by issue.Project into issues
                          select new
                          {
                              TotalIssues = issues.Count(),
                              UnsolvedIssues = issues.Where(g => !g.Solved).Count(),
                              SolvedIssues = issues.Where(g => g.Solved).Count(),
                          };

                if (res.Count() > 0)
                {
                    var resf = res.First();
                    return new ProjectStatistics(id, name, resf.TotalIssues, resf.UnsolvedIssues, resf.SolvedIssues);
                }
                else
                {
                    return new ProjectStatistics(id, name, 0, 0, 0);
                }
            }
            catch (Exception)
            {
                throw new ProjectManagementException($"Project #{projectID} doesn't exist!");
            }
        }

        /// <inheritdoc/>
        public IEnumerable<UserStatistics> GetUserStatistics()
        {
            var issueRepo = this.dataAccess.GetIssuesRepository();
            var userRepo = this.dataAccess.GetUsersRepository();

            var res = from issue in issueRepo.ReadAll()
                      group issue by issue.AssignedTo into groups
                      where groups.Key.HasValue
                      select new
                      {
                          ID = groups.Key.Value,
                          AllIssues = groups.Count(),
                          UnsolvedIssues = groups.Where(g => !g.Solved).Count(),
                          SolvedIssues = groups.Where(g => g.Solved).Count(),
                      };

            foreach (var result in res)
            {
                yield return new UserStatistics(result.ID, userRepo.GetById(result.ID).Username, result.AllIssues, result.UnsolvedIssues, result.SolvedIssues);
            }
        }
    }
}
