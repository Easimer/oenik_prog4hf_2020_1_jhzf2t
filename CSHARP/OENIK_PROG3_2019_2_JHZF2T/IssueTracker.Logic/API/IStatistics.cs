﻿// <copyright file="IStatistics.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker
{
    using System.Collections.Generic;

    /// <summary>
    /// Issue tracker statistics interface.
    /// </summary>
    public interface IStatistics
    {
        /// <summary>
        /// Retrieve statistics about all projects.
        /// </summary>
        /// <returns>A generator of all project stats.</returns>
        IEnumerable<ProjectStatistics> GetProjectStatistics();

        /// <summary>
        /// Retrieve statistics about a single project.
        /// </summary>
        /// <param name="projectID">Project identifier.</param>
        /// <returns>The project stats.</returns>
        /// <exception cref="ProjectManagementException">Thrown if the project doesn't exist.</exception>
        ProjectStatistics GetProjectStatistics(int projectID);

        /// <summary>
        /// Retrieve statistics about all users.
        /// </summary>
        /// <returns>A generator of all user stats.</returns>
        IEnumerable<UserStatistics> GetUserStatistics();
    }
}
