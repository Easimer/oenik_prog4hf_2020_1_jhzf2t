﻿// <copyright file="ClientIssueManagement.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System;
    using Net.Easimer.Prog3.IssueTracker;

    /// <summary>
    /// Client issue management.
    /// </summary>
    internal partial class Client
    {
        private void DisplayIssueTable(IIssueTracker issueTracker, IssueSelectionWidget issueTable, bool noCreate = true, int projectID = -1)
        {
            bool done = false;
            var issueMgmt = issueTracker.GetIssueManagement();
            string[] menuItems = { "---", "Create", "Delete", "Modify", "Refresh", "Close", "---", "Assign", "Search", "Back" };
            string[] menuItemsNoCreate = { "---", "---", "Delete", "Modify", "Refresh", "Close", "---", "Assign", "Search", "Back" };

            if (noCreate)
            {
                menuItems = menuItemsNoCreate;
            }

            while (!done)
            {
                ImTUI.Begin();

                if (!issueTable.Update())
                {
                    ImTUI.Text("The list is empty!");
                }

                switch (ImTUI.MenuBar(menuItems))
                {
                    case 2:
                        if (!noCreate)
                        {
                            this.CreateNewIssue(projectID);
                        }

                        break;
                    case 3:
                        if (issueTable.SelectedEntity != null)
                        {
                            issueMgmt.DeleteIssue(issueTable.GetSelectedEntity<IssueInfo>().ID);
                        }

                        break;
                    case 5:
                        issueTable.InvalidateCache();
                        break;
                    case 6:
                        if (issueTable.SelectedEntity != null)
                        {
                            issueMgmt.CloseIssue(issueTable.GetSelectedEntity<IssueInfo>().ID);
                        }

                        break;
                    case 8:
                        if (issueTable.SelectedEntity != null)
                        {
                            issueMgmt.AssignIssue(issueTable.GetSelectedEntity<IssueInfo>().ID, this.SelectAUser(this.issueTracker).ID);
                        }

                        break;
                    case 9:
                        this.SearchIssues(issueTracker, noCreate, projectID);
                        break;
                    case 10:
                        done = true;
                        this.SwitchScreen(Screen.ProjectManagement);
                        break;
                }

                ImTUI.End(!done);
            }
        }

        private void SearchIssues(IIssueTracker issueTracker, bool noCreate = true, int projectID = -1)
        {
            string filter = string.Empty;
            bool done = false;
            bool ok = false;

            ImTUI.ForceClear();

            while (!done)
            {
                ImTUI.Begin();

                ImTUI.Input("Search", ref filter);
                if (ImTUI.Button("OK"))
                {
                    ok = done = true;
                }
                else if (ImTUI.Button("Cancel"))
                {
                    done = true;
                }

                ImTUI.End(!done);
            }

            if (ok)
            {
                done = false;
                FilteredIssueSelectionWidget table;
                if (noCreate)
                {
                    table = new FilteredIssueSelectionWidget(issueTracker, filter);
                }
                else
                {
                    table = new FilteredIssueSelectionWidget(issueTracker, projectID, filter);
                }

                ImTUI.ForceClear();

                this.DisplayIssueTable(issueTracker, table, noCreate, projectID);
            }

            ImTUI.ForceClear();
        }

        private void IssueManagement(IssueTrackerSession session, int projectID)
        {
            var issueTable = new IssueSelectionWidget(this.issueTracker, projectID);

            ImTUI.ResetScreen();

            this.DisplayIssueTable(this.issueTracker, issueTable, false, projectID);
        }

        private void CreateNewIssue(int projectID)
        {
            bool done = false;
            bool ok = false;
            IssueInfo res;
            IssueInfo newIssue = new IssueInfo();
            var issueMgmt = this.issueTracker.GetIssueManagement();
            var projMgmt = this.issueTracker.GetProjectManagement();
            newIssue.ProjectRef = projMgmt.GetByID(projectID);

            ImTUI.ForceClear();

            while (!done)
            {
                ImTUI.Begin();

                EntityEditor.Edit(newIssue, this.issueTracker);

                ImTUI.Text("\n");
                if (ImTUI.Button("OK"))
                {
                    done = true;
                    ok = true;
                }
                else if (ImTUI.Button("Cancel"))
                {
                    done = true;
                    ok = false;
                }

                ImTUI.End(!done);
            }

            if (ok)
            {
                ImTUI.Begin();
                try
                {
                    res = issueMgmt.CreateNewIssue(projectID, newIssue.Title, newIssue.AssignedTo);
                    ImTUI.MessageBox($"Issue #{res.ID} titled '{res.Title}' has been created!");
                }
                catch (ProjectManagementException pex)
                {
                    ImTUI.MessageBox($"There is a problem with the currently selected project: {pex.Message}");
                }
                catch (IssueManagementException iex)
                {
                    ImTUI.MessageBox($"An error orrcured while creating a new issue: {iex.Message}");
                }

                ImTUI.End(true);
            }

            ImTUI.ResetScreen();
        }

        private void IssueManagement(IssueTrackerSession session)
        {
            var issueTable = new IssueSelectionWidget(this.issueTracker);
            this.DisplayIssueTable(this.issueTracker, issueTable);
            this.SwitchScreen(Screen.MainMenu);
        }
    }
}
