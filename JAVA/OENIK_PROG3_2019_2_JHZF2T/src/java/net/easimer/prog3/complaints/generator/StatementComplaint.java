package net.easimer.prog3.complaints.generator;

import java.util.Random;

/**
 *
 * @author danielm
 */
public class StatementComplaint implements BaseStatement {
    private BaseStatement part0, part1, part2;
    
    public StatementComplaint(Random rnd) {
        BaseStatement doesntWork = null, swearing = null;
        
        if(rnd.nextDouble() < 0.50) {
            doesntWork = new StatementDoesntWork(rnd);
        }
        if(rnd.nextDouble() < 0.10) {
            swearing = new StatementSwearing(rnd);
        }
        
        part0 = new StatementOptional(doesntWork);
        part1 = rnd.nextDouble() < 0.5 ?
                new StatementAttempted(rnd) :
                new StatementDoesSomethingElse(rnd);
        part2 = new StatementOptional(swearing);
    }
    
    @Override
    public String getStatementText() {
        return String.format("%s %s %s",
                part0.getStatementText(),
                part1.getStatementText(),
                part2.getStatementText());
    }
    
}
