﻿// <copyright file="StatisticsTests.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Test
{
    using System.Linq;
    using Net.Easimer.Prog3.IssueTracker;
    using NUnit.Framework;

    /// <summary>
    /// Tests for IStatistics.
    /// </summary>
    internal class StatisticsTests : LogicTestEnvironment
    {
        /// <summary>
        /// Test for IStatistics::GetProjectStatistics(int).
        /// </summary>
        [Test]
        public void SingleProjectStatistics()
        {
            this.ClearRepositories();

            foreach (var proj in this.GenerateProjects())
            {
                this.RepoProj.Entities.AddLast(proj);
            }

            foreach (var issue in this.GenerateTestIssues())
            {
                this.RepoIssue.Entities.AddLast(issue);
            }

            var stats = this.Stats.GetProjectStatistics(1);
            Assert.AreEqual(1, stats.ProjectID);
            Assert.AreEqual(this.PMan.GetByID(1).Name, stats.ProjectName);
            Assert.AreEqual(2, stats.TotalIssues);
            Assert.AreEqual(1, stats.UnsolvedIssues);
            Assert.AreEqual(1, stats.SolvedIssues);

            Assert.Throws<ProjectManagementException>(() => this.Stats.GetProjectStatistics(999));

            var multistats = this.Stats.GetProjectStatistics();
            foreach (var stat in multistats)
            {
                switch (stat.ProjectID)
                {
                    case 1:
                        Assert.AreEqual(1, stat.ProjectID);
                        Assert.AreEqual(this.PMan.GetByID(1).Name, stat.ProjectName);
                        Assert.AreEqual(2, stat.TotalIssues);
                        Assert.AreEqual(1, stat.UnsolvedIssues);
                        Assert.AreEqual(1, stat.SolvedIssues);
                        break;
                    default:
                        Assert.AreEqual(this.PMan.GetByID(stat.ProjectID).Name, stat.ProjectName);
                        Assert.AreEqual(0, stat.TotalIssues);
                        Assert.AreEqual(0, stat.UnsolvedIssues);
                        Assert.AreEqual(0, stat.SolvedIssues);
                        break;
                }
            }
        }

        /// <summary>
        /// Test for IStatistics::GetProjectStatistics(int) when a project exists but has no issues.
        /// </summary>
        [Test]
        public void SingleProjectWithoutIssuesStatistics()
        {
            this.ClearRepositories();

            foreach (var proj in this.GenerateProjects())
            {
                this.RepoProj.Entities.AddLast(proj);
            }

            foreach (var issue in this.GenerateTestIssues())
            {
                this.RepoIssue.Entities.AddLast(issue);
            }

            var stats = this.Stats.GetProjectStatistics(2);

            var proj2 = this.RepoProj.GetById(2);

            Assert.That(stats.ProjectID, Is.EqualTo(proj2.ID));
            Assert.That(stats.ProjectName, Is.EqualTo(proj2.Name));
            Assert.That(stats.TotalIssues, Is.EqualTo(0));
            Assert.That(stats.UnsolvedIssues, Is.EqualTo(0));
            Assert.That(stats.SolvedIssues, Is.EqualTo(0));
        }

        /// <summary>
        /// Tests user statistics.
        /// </summary>
        [Test]
        public void UserStatistics()
        {
            this.ClearRepositories();

            this.RepoUser.Create(
            new Database.Data.User
            {
                ID = 1, Username = "user1", FirstName = "Teszt 1", LastName = "Elek",
            },
            new Database.Data.User
            {
                ID = 2, Username = "user2", FirstName = "Teszt 2", LastName = "Elek",
            });

            this.RepoProj.Create(
                new Database.Data.Project
                {
                    ID = 1, Name = "Project 1",
                    Descript = string.Empty, Lang = string.Empty, Homepage = string.Empty,
                });

            this.RepoIssue.Create(
            new Database.Data.Issue
            {
                ID = 1, Title = "issue1", Project = 1, AssignedTo = 1, Solved = false,
            },
            new Database.Data.Issue
            {
                ID = 2, Title = "issue2", Project = 1, AssignedTo = 1, Solved = true,
            },
            new Database.Data.Issue
            {
                ID = 3, Title = "issue3", Project = 1, AssignedTo = 2, Solved = false,
            },
            new Database.Data.Issue
            {
                ID = 4, Title = "issue4", Project = 1, AssignedTo = 2, Solved = true,
            },
            new Database.Data.Issue
            {
                ID = 5, Title = "issue5", Project = 1, AssignedTo = null, Solved = false,
            });

            var stats = this.Stats.GetUserStatistics();

            Assert.That(stats.All(u => u.TotalIssues == 2 && u.SolvedIssues == 1 && u.UnsolvedIssues == 1));
        }
    }
}
