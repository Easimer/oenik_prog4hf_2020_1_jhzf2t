﻿// <copyright file="ImTUI.Backbuffer.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System;
    using System.Collections.Generic;
    using System.Threading;

    /// <content>
    /// Implementation of the backbuffer used by ImTUI.
    /// </content>
    public static partial class ImTUI
    {
        /// <summary>
        /// Class holding optional console key information.
        /// </summary>
        public class ConsoleKeyInfo
        {
            private bool present;
            private System.ConsoleKeyInfo kv;

            /// <summary>
            /// Initializes a new instance of the <see cref="ConsoleKeyInfo"/> class.
            /// </summary>
            public ConsoleKeyInfo()
            {
                this.present = false;
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="ConsoleKeyInfo"/> class.
            /// </summary>
            /// <param name="kv">KeyInfo.</param>
            public ConsoleKeyInfo(System.ConsoleKeyInfo kv)
            {
                this.present = true;
                this.kv = kv;
            }

            /// <summary>
            /// Gets the virtual key.
            /// </summary>
            public ConsoleKey Key => this.present ? this.kv.Key : throw new Exception();

            /// <summary>
            /// Gets the key modifiers.
            /// </summary>
            public ConsoleModifiers Modifiers => this.kv.Modifiers;

            /// <summary>
            /// Gets the Unicode character represented by the key.
            /// </summary>
            public char KeyChar => this.kv.KeyChar;

            /// <summary>
            /// Gets the underlying .NET keyinfo struct.
            /// </summary>
            public System.ConsoleKeyInfo Underlying => this.kv;

            /// <summary>
            /// Gets a value indicating whether the information is present.
            /// </summary>
            public bool Present => this.present;
        }

        /// <summary>
        /// Backbuffer for ImTUI
        /// Buffer the screen contents to prevent
        /// overdraw (Console.* is slow).
        /// </summary>
        private class Backbuffer
        {
            /// <summary>
            /// The array backing this Backbuffer.
            /// </summary>
            private char[] buffer;
            private char[] bufferNormal;
            private char[] bufferInverted;
            private int curX;
            private int curY;
            private int width;
            private int height;
            private bool redraw;
            private int framesSinceKeyRead;
            private bool isInverted;

            private RedrawRegion rrNormal;
            private RedrawRegion rrInverted;

            private LinkedList<ConsoleKeyInfo> keyFIFO;

            /// <summary>
            /// Initializes a new instance of the <see cref="Backbuffer"/> class.
            /// It's size will be width*height.
            /// </summary>
            /// <param name="width">Width of the buffer.</param>
            /// <param name="height">Height of the buffer.</param>
            public Backbuffer(int width, int height)
            {
                this.width = width;
                this.height = height;
                this.curX = this.curY = 0;
                this.bufferNormal = new char[width * height];
                this.bufferInverted = new char[width * height];
                this.buffer = this.bufferNormal;
                this.isInverted = false;
                this.redraw = false;
                this.rrNormal = new RedrawRegion(width, height);
                this.rrInverted = new RedrawRegion(width, height);
                this.keyFIFO = new LinkedList<ConsoleKeyInfo>();
                this.framesSinceKeyRead = 0;
                this.Clear();
            }

            /// <summary>
            /// Gets or sets the cursor's X position.
            /// </summary>
            public int CursorLeft
            {
                get => this.curX;
                set => this.curX = value;
            }

            /// <summary>
            /// Gets or sets the cursor's Y position.
            /// </summary>
            public int CursorTop
            {
                get => this.curY;
                set => this.curY = value;
            }

            /// <summary>
            /// Gets the width of the window.
            /// </summary>
            public int WindowWidth => this.width;

            /// <summary>
            /// Gets the height of the window.
            /// </summary>
            public int WindowHeight => this.height;

            /// <summary>
            /// Gets a value indicating whether any key input is available.
            /// </summary>
            public bool KeyAvailable
            {
                get
                {
                    if (this.keyFIFO.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return Console.KeyAvailable;
                    }
                }
            }

            private int RedrawMinX => this.isInverted ? this.rrInverted.RedrawMinX : this.rrNormal.RedrawMinX;

            private int RedrawMinY => this.isInverted ? this.rrInverted.RedrawMinY : this.rrNormal.RedrawMinY;

            private int RedrawMaxX => this.isInverted ? this.rrInverted.RedrawMaxX : this.rrNormal.RedrawMaxX;

            private int RedrawMaxY => this.isInverted ? this.rrInverted.RedrawMaxY : this.rrNormal.RedrawMaxY;

            /// <summary>
            /// Begin a new frame.
            /// </summary>
            public void Begin()
            {
                // Clear old frame's dirty region.
                for (int y = this.RedrawMinY; y <= this.RedrawMaxY; y++)
                {
                    Console.CursorTop = y;
                    Console.CursorLeft = this.RedrawMinX;
                    int off_y = y * this.width;
                    for (int x = this.RedrawMinX; x <= this.RedrawMaxX; x++)
                    {
                        this.buffer[off_y + x] = ' ';
                    }
                }
            }

            public void End()
            {
                this.rrNormal.Reset();
                this.rrInverted.Reset();
                this.framesSinceKeyRead++;

                // If the client didn't read a key for a while we flush the key queue
                if (this.framesSinceKeyRead > 8)
                {
                    this.keyFIFO.Clear();
                    this.framesSinceKeyRead = 0;
                }
            }

            /// <summary>
            /// Write a single character at the cursor position.
            /// </summary>
            /// <param name="ch">Character to write.</param>
            public void Write(char ch)
            {
                switch (ch)
                {
                    case '\n':
                        this.CarriageReturn();
                        break;
                    case '\r':
                        break;
                    default:
                        int off_ch = (this.curY * this.width) + this.curX;
                        char old_ch = this.buffer[off_ch];
                        if (old_ch != ch)
                        {
                            this.buffer[off_ch] = ch;
                            this.ForceRedraw();
                            this.RecalcRedrawRegion(this.curX, this.curY);
                        }

                        this.StepCursor();
                        break;
                }
            }

            /// <summary>
            /// Write a formatted string.
            /// </summary>
            /// <param name="format">Format string.</param>
            /// <param name="a">Arguments.</param>
            public void Write(string format, params object[] a)
            {
                string s = string.Format(format, a);
                for (int i = 0; i < s.Length; i++)
                {
                    this.Write(s[i]);
                }
            }

            /// <summary>
            /// Write a formatted string and do an lfcr.
            /// </summary>
            /// <param name="format">Format string.</param>
            /// <param name="a">Arguments.</param>
            public void WriteLine(string format, params object[] a)
            {
                this.Write(format, a);
                this.CarriageReturn();
            }

            /// <summary>
            /// Determines whether the buffer be redrawn.
            /// </summary>
            /// <returns>Should the buffer be redrawn.</returns>
            public bool ShouldBeRedrawn()
            {
                bool ret = this.redraw;
                this.redraw = false;
                return ret;
            }

            /// <summary>
            /// Clear entire backbuffer and reset cursor.
            /// NOTE(danielm): don't call this every frame! If you want
            /// to reset the cursor, call Reposition().
            /// </summary>
            public void Clear()
            {
                for (int i = 0; i < this.width * this.height; i++)
                {
                    this.buffer[i] = ' ';
                }

                this.curX = this.curY = 0;

                this.rrNormal.Stretch();

                this.Redraw();

                this.rrNormal.Shrink();
            }

            /// <summary>
            /// Reset cursor.
            /// </summary>
            public void Reposition()
            {
                this.curX = this.curY = 0;
            }

            /// <summary>
            /// Set the redraw flag.
            /// </summary>
            public void ForceRedraw()
            {
                if (!this.redraw)
                {
                    this.redraw = true;
                }
            }

            /// <summary>
            /// Draw what has to be redraw to the console.
            /// </summary>
            /// <param name="debugRedrawRegion">[DEBUG] Visualize redraw region.</param>
            public void Redraw(bool debugRedrawRegion = false)
            {
                int off_y;
                if (debugRedrawRegion)
                {
                    Console.BackgroundColor = ConsoleColor.Red;
                }

                this.RedrawBuffer(ConsoleColor.Black, ConsoleColor.Gray, this.rrNormal, this.bufferNormal);
                this.RedrawBuffer(ConsoleColor.Gray, ConsoleColor.Black, this.rrInverted, this.bufferInverted);

                // Redraw special region
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.CursorTop = this.height - 1;
                Console.CursorLeft = 0;
                off_y = (this.height - 1) * this.width;
                for (int x = 0; x < this.width - 1; x++)
                {
                    Console.Write(this.buffer[off_y + x]);
                }

                if (debugRedrawRegion)
                {
                    Console.BackgroundColor = ConsoleColor.Black;
                }
            }

            /// <summary>
            /// Read input from the keyboard. The keypress (if ever happens)
            /// won't be echoed to the screen.
            /// The keys can be filtered by the filter parameter.
            /// </summary>
            /// <param name="filter">Input filter.</param>
            /// <param name="block">Block execution.</param>
            /// <returns>Key press info.</returns>
            public ConsoleKeyInfo ReadKey(KeyFilter filter = KeyFilter.AllowAll, bool block = true)
            {
                ConsoleKeyInfo ret = new ConsoleKeyInfo();
                bool done = false;

                this.framesSinceKeyRead = 0;

                // Scan through key queue to check if we already something that matches
                var cur = this.keyFIFO.First;
                while (cur != null)
                {
                    if (this.FilterAllowsKey(cur.Value.Underlying, filter))
                    {
                        ret = cur.Value;
                        this.keyFIFO.Remove(cur);
                        return ret;
                    }

                    cur = cur.Next;
                }

                while (block && !done)
                {
                    var kv = Console.ReadKey(true);
                    if (ImTUI.HandleKeyInput(new ConsoleKeyInfo(kv)))
                    {
                        if (this.FilterAllowsKey(kv, filter))
                        {
                            ret = new ConsoleKeyInfo(kv);
                            done = true;
                        }
                        else
                        {
                            this.keyFIFO.AddLast(new ConsoleKeyInfo(kv));
                        }
                    }
                    else
                    {
                        done = true;
                    }
                }

                return ret;
            }

            /// <summary>
            /// Peek into the input queue. The key read will be put back
            /// into the queue. To remove it use <see cref="ReadKey(KeyFilter, bool)"/>.
            /// </summary>
            /// <returns>Key info.</returns>
            public ConsoleKeyInfo PeekKey(bool block = false)
            {
                // Scan through key queue to check if we already something that matches
                if (this.keyFIFO.Count > 0)
                {
                    return this.keyFIFO.First.Value;
                }

                while (block && !Console.KeyAvailable)
                {
                    Thread.Sleep(32);
                }

                if (Console.KeyAvailable)
                {
                    var kv = Console.ReadKey(true);
                    var kvinf = new ConsoleKeyInfo(kv);
                    if (ImTUI.HandleKeyInput(kvinf))
                    {
                        this.keyFIFO.AddLast(kvinf);
                        return this.keyFIFO.Last.Value;
                    }
                }

                return new ConsoleKeyInfo();
            }

            /// <summary>
            /// Discard buffered key inputs.
            /// Use this when you switch between screens and don't want the user input
            /// from the previous screen to carry over.
            /// </summary>
            public void DiscardBufferedInput()
            {
                this.keyFIFO.Clear();
            }

            /// <summary>
            /// Invers the colors of the screen.
            /// DON'T USE THIS.
            /// </summary>
            public void InvertScreen()
            {
                this.isInverted = !this.isInverted;
            }

            /// <summary>
            /// Sets the colors of the screen back to normal.
            /// DON'T USE THIS.
            /// </summary>
            public void NormalScreen()
            {
                this.isInverted = false;
            }

            /// <summary>
            /// INTERNAL API: check if a key should be allowed or filtered.
            /// </summary>
            private bool FilterAllowsKey(System.ConsoleKeyInfo kv, KeyFilter filter)
            {
                bool ret = false;

                if (filter == KeyFilter.AllowAll)
                {
                    ret = true;
                }
                else
                {
                    if (filter.HasFlag(KeyFilter.Displayables))
                    {
                        // NOTE(danielm): per MSDN docs, if a key is non-displayable
                        // in Unicode, KeyChar will have the value '\u0000'.
                        ret = kv.KeyChar != '\u0000';
                    }
                    else if (filter.HasFlag(KeyFilter.FunctionKeys))
                    {
                        // NOTE(danielm): we don't allow fkeys beyond F12
                        ret = kv.Key >= ConsoleKey.F1 && kv.Key <= ConsoleKey.F12;
                    }
                    else if (filter.HasFlag(KeyFilter.Tab))
                    {
                        ret = kv.Key == ConsoleKey.Tab;
                    }
                }

                return ret;
            }

            private void RedrawBuffer(ConsoleColor bg, ConsoleColor fg, RedrawRegion rr, char[] buffer)
            {
                if (!rr.IsReset())
                {
                    int off_y;
                    Console.BackgroundColor = bg;
                    Console.ForegroundColor = fg;
                    for (int y = rr.RedrawMinY; y <= rr.RedrawMaxY; y++)
                    {
                        Console.CursorTop = y;
                        Console.CursorLeft = rr.RedrawMinX;
                        off_y = y * this.width;
                        for (int x = rr.RedrawMinX; x <= rr.RedrawMaxX; x++)
                        {
                            Console.Write(buffer[off_y + x]);
                        }
                    }
                }
            }

            /// <summary>
            /// INTERNAL API: step the cursor.
            /// </summary>
            private void StepCursor()
            {
                this.curX++;
                if (this.curX == this.width)
                {
                    this.CarriageReturn();
                }
            }

            /// <summary>
            /// INTERNAL API: do a crlf.
            /// </summary>
            private void CarriageReturn()
            {
                int off_base = this.curY * this.width;
                for (int x = this.curX; x < this.width; x++)
                {
                    int off_ch = off_base + x;
                    if (this.buffer[off_ch] != ' ')
                    {
                        this.RecalcRedrawRegion(x, this.curY);
                    }

                    this.buffer[off_ch] = ' ';
                }

                this.curY++;
                this.curX = 0;
                if (this.curY == this.height)
                {
                    this.curY = this.height - 1;
                }
            }

            /// <summary>
            /// INTERNAL API: recalculate boundaries of the region
            /// that has to be redrawn.
            /// </summary>
            /// <param name="x">X coords.</param>
            /// <param name="y">Y coords.</param>
            private void RecalcRedrawRegion(int x, int y)
            {
                if (this.isInverted)
                {
                    this.rrInverted.Expand(x, y);
                }
                else
                {
                    this.rrNormal.Expand(x, y);
                }
            }

            private struct RedrawRegion
            {
                public int RedrawMinX; public int RedrawMinY;
                public int RedrawMaxX; public int RedrawMaxY;

                private int width;
                private int height;

                public RedrawRegion(int width, int height)
                {
                    this.RedrawMinX = this.RedrawMinY = this.RedrawMaxX = this.RedrawMaxY = 0;
                    this.width = width;
                    this.height = height;
                }

                public void Stretch()
                {
                    this.RedrawMinX = this.RedrawMinY = 0;

                    this.RedrawMaxX = this.width - 1;
                    this.RedrawMaxY = this.height - 1;
                }

                public void Shrink()
                {
                    this.RedrawMinX = this.RedrawMinY = int.MaxValue;
                    this.RedrawMaxX = this.RedrawMaxY = 0;
                }

                public void Reset()
                {
                    this.RedrawMinX = this.RedrawMinY = this.RedrawMaxX = this.RedrawMaxY = 0;
                }

                public bool IsReset()
                {
                    return this.RedrawMinX == 0 && this.RedrawMinY == 0 && this.RedrawMaxX == 0 && this.RedrawMaxY == 0;
                }

                public void Expand(int x, int y)
                {
                    if (x < this.RedrawMinX)
                    {
                        this.RedrawMinX = x;
                    }

                    if (x > this.RedrawMaxX)
                    {
                        this.RedrawMaxX = x;
                    }

                    // NOTE(danielm): last line is a special region that is redrawn every Redraw() call
                    if (y != this.height - 1)
                    {
                        if (y < this.RedrawMinY)
                        {
                            this.RedrawMinY = y;
                        }

                        if (y > this.RedrawMaxY)
                        {
                            this.RedrawMaxY = y;
                        }
                    }
                }
            }
        }
    }
}
