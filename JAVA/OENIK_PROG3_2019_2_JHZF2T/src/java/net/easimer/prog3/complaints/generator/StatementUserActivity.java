package net.easimer.prog3.complaints.generator;

import java.util.Random;

/**
 *
 * @author danielm
 */
public class StatementUserActivity implements BaseStatement {
    private BaseStatement m_entity;
    private String m_activity;
    
    private static String[] activities = new String[] {
        "hozzáadni egy új %s", "törölni egy %s", "megkeresni egy %s",
        "lekérdezni több %s"
    };
    
    public StatementUserActivity(Random rnd) {
        m_activity = activities[rnd.nextInt(activities.length)];
        m_entity = new StatementWhatEntity(rnd);
    }
    
    @Override
    public String getStatementText() {
        return String.format(m_activity, m_entity.getStatementText());
    }
}
