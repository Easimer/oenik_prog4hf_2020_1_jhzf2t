﻿// <copyright file="UserStatistics.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker
{
    /// <summary>
    /// User statistics.
    /// </summary>
    public struct UserStatistics
    {
        /// <summary>
        /// Identifier of the project.
        /// </summary>
        public int UserID;

        /// <summary>
        /// Name of the project.
        /// </summary>
        public string UserName;

        /// <summary>
        /// Number of issues assigned to this user.
        /// </summary>
        public int TotalIssues;

        /// <summary>
        /// Number of solved issues.
        /// </summary>
        public int SolvedIssues;

        /// <summary>
        /// Number of unsolved issues.
        /// </summary>
        public int UnsolvedIssues;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserStatistics"/> struct.
        /// </summary>
        /// <param name="id">User identifier.</param>
        /// <param name="name">Project name.</param>
        /// <param name="totalIssues">Number of total issues.</param>
        /// <param name="unsolvedIssues">Number of unsolved issues.</param>
        /// <param name="solvedIssues">Number of solved issues.</param>
        public UserStatistics(int id, string name, int totalIssues, int unsolvedIssues, int solvedIssues)
        {
            this.UserID = id;
            this.UserName = name;
            this.TotalIssues = totalIssues;
            this.UnsolvedIssues = unsolvedIssues;
            this.SolvedIssues = solvedIssues;
        }
    }
}
