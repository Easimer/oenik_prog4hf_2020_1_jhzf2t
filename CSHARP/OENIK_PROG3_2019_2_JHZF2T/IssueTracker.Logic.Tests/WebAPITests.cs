﻿// <copyright file="WebAPITests.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Test
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using Moq;
    using Moq.Protected;
    using Net.Easimer.Prog3.IssueTracker;
    using Net.Easimer.Prog3.IssueTracker.Impl;
    using NUnit.Framework;

    /// <summary>
    /// Tests the Web API client.
    /// </summary>
    internal class WebAPITests
    {
        /// <summary>
        /// Tests the Authentication request.
        /// </summary>
        [Test]
        public void TestAuthentication()
        {
            Mock<HttpMessageHandler> handlerMock;
            HttpClient httpClient;

            this.CreateDumbClientReceiving(
                @"<?xml version=""1.0"" encoding=""UTF-8"" standalone=""yes""?>
                <AuthenticationResult>
                    <Status>true</Status>
                    <UserID>1</UserID>
                </AuthenticationResult>",
                out handlerMock,
                out httpClient);

            var client = new WebProg3Impl(httpClient);
            int userId = -1;

            Assert.DoesNotThrow(() => userId = client.Authenticate("test", "test"));
            Assert.That(userId, Is.EqualTo(1));
            var expectedUri = new Uri("http://example.com/IWebProg3/Authenticate/v1");

            handlerMock.Protected().Verify(
                "SendAsync",
                Times.Exactly(1),
                ItExpr.Is<HttpRequestMessage>(req =>
                    req.Method == HttpMethod.Post &&
                    req.RequestUri == expectedUri),
                ItExpr.IsAny<CancellationToken>());
        }

        /// <summary>
        /// Tests authentication using invalid credentials.
        /// </summary>
        [Test]
        public void TestAuthenticationInvalidCredentials()
        {
            Mock<HttpMessageHandler> handlerMock;
            HttpClient httpClient;

            this.CreateDumbClientReceiving(
                @"<?xml version=""1.0"" encoding=""UTF-8"" standalone=""yes""?>
                <AuthenticationResult>
                    <Status>false</Status>
                    <UserID>-1</UserID>
                    <Message>msg</Message>
                    <ErrorCode>EACCES</ErrorCode>
                </AuthenticationResult>",
                out handlerMock,
                out httpClient);

            var client = new WebProg3Impl(httpClient);

            var ex = Assert.Throws<AuthenticationException>(() => client.Authenticate("test", "test"));
            var expectedUri = new Uri("http://example.com/IWebProg3/Authenticate/v1");

            // TODO(danielm): test ErrorCode when it's eventually included in the exception.
            Assert.That(ex.Message, Is.EqualTo("msg"));

            handlerMock.Protected().Verify(
                "SendAsync",
                Times.Exactly(1),
                ItExpr.Is<HttpRequestMessage>(req =>
                    req.Method == HttpMethod.Post &&
                    req.RequestUri == expectedUri),
                ItExpr.IsAny<CancellationToken>());
        }

        /// <summary>
        /// Tests GetComplaint.
        /// </summary>
        [Test]
        public void TestComplaintsProcessing()
        {
            Mock<HttpMessageHandler> handlerMock;
            HttpClient httpClient;

            this.CreateDumbClientReceiving(
                @"<?xml version=""1.0"" encoding=""UTF-8"" standalone=""yes""?>
                <ComplaintsResult>
                    <Complaint>
                        <Author>Author 1</Author>
                        <Contents>Contents 1</Contents>
                    </Complaint>
                    <Complaint>
                        <Author>Author 2</Author>
                        <Contents>Contents 2</Contents>
                    </Complaint>
                </ComplaintsResult>",
                out handlerMock,
                out httpClient);

            var client = new WebProg3Impl(httpClient);
            IEnumerable<Complaint> complaints = null;

            Assert.DoesNotThrow(() => complaints = client.GetComplaints());
            Assert.That(complaints, Is.Not.Null);
            var enumer = complaints.GetEnumerator();
            Assert.That(enumer, Is.Not.Null);

            // Element 1
            Assert.That(enumer.MoveNext(), Is.True);
            Assert.That(enumer.Current, Is.Not.Null);
            Assert.That(enumer.Current.Author, Is.EqualTo("Author 1"));
            Assert.That(enumer.Current.Contents, Is.EqualTo("Contents 1"));

            // Element 2
            Assert.That(enumer.MoveNext(), Is.True);
            Assert.That(enumer.Current, Is.Not.Null);
            Assert.That(enumer.Current.Author, Is.EqualTo("Author 2"));
            Assert.That(enumer.Current.Contents, Is.EqualTo("Contents 2"));

            // EOF
            Assert.That(enumer.MoveNext(), Is.False);

            var expectedUri = new Uri("http://example.com/IWebProg3/GetComplaints/v1");

            handlerMock.Protected().Verify(
                "SendAsync",
                Times.Exactly(1),
                ItExpr.Is<HttpRequestMessage>(req =>
                    req.Method == HttpMethod.Get &&
                    req.RequestUri == expectedUri),
                ItExpr.IsAny<CancellationToken>());
        }

        /// <summary>
        /// Tests GetComplaints with invalid data returned by the server.
        /// </summary>
        [Test]
        public void GetComplaintsInvalidData()
        {
            Mock<HttpMessageHandler> handlerMock;
            HttpClient httpClient;

            this.CreateDumbClientReceiving(
                @"<?yml bersion=""1.0"" incoding=""UTF-8"" standalong=""yes""?>
                <Complaints eeegeeee Result>
                    <ALKDSComp:DS:LKlaint>
                        <Author>Author 1</Author>
                </ComplaintddsdsResult>",
                out handlerMock,
                out httpClient);

            var client = new WebProg3Impl(httpClient);

            Assert.Throws<Exception>(() =>
            {
                foreach (var complaint in client.GetComplaints())
                {
                }
            });

            var expectedUri = new Uri("http://example.com/IWebProg3/GetComplaints/v1");

            handlerMock.Protected().Verify(
                "SendAsync",
                Times.Exactly(1),
                ItExpr.Is<HttpRequestMessage>(req =>
                    req.Method == HttpMethod.Get &&
                    req.RequestUri == expectedUri),
                ItExpr.IsAny<CancellationToken>());
        }

        /// <summary>
        /// Tests authentication if there was a network error.
        /// </summary>
        [Test]
        public void TestNetworkErrorAuthentication()
        {
            this.TestNetworkError<AuthenticationException>(client => client.Authenticate("test", "test"));
        }

        /// <summary>
        /// Tests complaint retrieving if there was a network error.
        /// </summary>
        [Test]
        public void TestNetworkErrorGetComplaints()
        {
            this.TestNetworkError<Exception>(client =>
            {
                foreach (var i in client.GetComplaints())
                {
                }
            });
        }

        /// <summary>
        /// Tests authentication if there was a HTTP error.
        /// </summary>
        [Test]
        public void TestHTTPErrorAuthentication()
        {
            this.TestHttpError<AuthenticationException>(client => client.Authenticate("test", "test"), HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Tests complaint retrieving if there was a HTTP error.
        /// </summary>
        [Test]
        public void TestHTTPErrorGetComplaints()
        {
            this.TestHttpError<Exception>(
                client =>
                {
                    foreach (var i in client.GetComplaints())
                    {
                    }
                },
                HttpStatusCode.NotFound);
        }

        private void TestNetworkError<T>(Action<WebProg3Impl> lam)
            where T : Exception
        {
            Mock<HttpMessageHandler> handlerMock;
            HttpClient httpClient;

            this.CreateDumbClientThrowing(out handlerMock, out httpClient);

            var client = new WebProg3Impl(httpClient);
            string expectedStrURI = string.Empty;

            Assert.Throws<T>(() => lam(client));

            handlerMock.Protected().Verify(
                "SendAsync",
                Times.Exactly(1),
                ItExpr.IsAny<HttpRequestMessage>(),
                ItExpr.IsAny<CancellationToken>());
        }

        private void TestHttpError<T>(Action<WebProg3Impl> lam, HttpStatusCode statusCode)
            where T : Exception
        {
            Mock<HttpMessageHandler> handlerMock;
            HttpClient httpClient;

            handlerMock = this.CreateMockHandlerReturningWith("Error 404", statusCode);
            httpClient = new HttpClient(handlerMock.Object)
            {
                BaseAddress = new Uri("http://example.com/IWebProg3/"),
            };

            var client = new WebProg3Impl(httpClient);
            string expectedStrURI = string.Empty;

            Assert.Throws<T>(() => lam(client));

            handlerMock.Protected().Verify(
                "SendAsync",
                Times.Exactly(1),
                ItExpr.IsAny<HttpRequestMessage>(),
                ItExpr.IsAny<CancellationToken>());
        }

        private Mock<HttpMessageHandler> CreateMockHandlerReturningWith(string content, HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock.Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage()
                {
                    StatusCode = statusCode,
                    Content = new StringContent(content),
                })
                .Verifiable();

            return handlerMock;
        }

        private void CreateDumbClientReceiving(string content, out Mock<HttpMessageHandler> handlerMock, out HttpClient httpClient)
        {
            handlerMock = this.CreateMockHandlerReturningWith(content);
            httpClient = new HttpClient(handlerMock.Object)
            {
                BaseAddress = new Uri("http://example.com/IWebProg3/"),
            };
        }

        private Mock<HttpMessageHandler> CreateMockHandlerThrowing()
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock.Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .Throws<Exception>()
                .Verifiable();

            return handlerMock;
        }

        private void CreateDumbClientThrowing(out Mock<HttpMessageHandler> handlerMock, out HttpClient httpClient)
        {
            handlerMock = this.CreateMockHandlerThrowing();
            httpClient = new HttpClient(handlerMock.Object)
            {
                BaseAddress = new Uri("http://example.com/IWebProg3/"),
            };
        }
    }
}
