﻿CREATE SCHEMA feleves;
GO

DROP TABLE IF EXISTS [feleves].Issues;
DROP TABLE IF EXISTS [feleves].Projects;
DROP TABLE IF EXISTS [feleves].Users;

CREATE TABLE [feleves].Users (
    [ID]        INT            IDENTITY (1, 1) NOT NULL,
    [Username]  NVARCHAR (64)  NOT NULL,
    [FirstName] NVARCHAR (128) NOT NULL,
    [LastName]  NVARCHAR (64)  NOT NULL,
    [EmailAddr] NVARCHAR (256) NOT NULL,
    [Telephone] NVARCHAR (32)  NOT NULL,
    CONSTRAINT [PK_feleves.Users] PRIMARY KEY CLUSTERED ([ID] ASC)
);

CREATE TABLE [feleves].Projects (
    [ID]           INT            IDENTITY (1, 1) NOT NULL,
    [Owner]        INT            NULL,
    [Name]         NVARCHAR (64)  NOT NULL,
    [Homepage]     NVARCHAR (256) NULL,
    [CreationDate] DATETIME       NOT NULL,
    [Parent]       INT            NULL,
	[Descript]  NVARCHAR (256) DEFAULT '' NOT NULL,
	[Lang]     NVARCHAR (64) NOT NULL,
    CONSTRAINT [PK_feleves.Projects] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_feleves.Projects_feleves.Users_Owner] FOREIGN KEY ([Owner]) REFERENCES [feleves].[Users] ([ID]) ON DELETE SET NULL,
    CONSTRAINT [FK_feleves.Projects_feleves.Projects_Parent] FOREIGN KEY ([Parent]) REFERENCES [feleves].[Projects] ([ID]) ON DELETE NO ACTION 
);

GO
CREATE NONCLUSTERED INDEX [IX_Owner]
    ON [feleves].[Projects]([Owner] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_Parent]
    ON [feleves].[Projects]([Parent] ASC);

CREATE TABLE [feleves].Issues (
    [ID]            INT            IDENTITY (1, 1) NOT NULL,
    [Project]       INT            NOT NULL,
    [Title]         NVARCHAR (512) NOT NULL,
    [AssignedTo]    INT            NULL,
    [Solved]        BIT            NOT NULL,
    [CreationDate]  DATETIME       DEFAULT GETDATE() NOT NULL,
	[Tag]           NVARCHAR (128) DEFAULT NULL,
	[Prio]          INT            DEFAULT 0,
    CONSTRAINT [PK_feleves.Issues] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_feleves.Issues_feleves.Users_AssignedTo] FOREIGN KEY ([AssignedTo]) REFERENCES [feleves].[Users] ([ID]) ON DELETE SET NULL,
    CONSTRAINT [FK_feleves.Issues_feleves.Projects_Project] FOREIGN KEY ([Project]) REFERENCES [feleves].[Projects] ([ID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_AssignedTo]
    ON [feleves].[Issues]([AssignedTo] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Project]
    ON [feleves].[Issues]([Project] ASC);

SET IDENTITY_INSERT [feleves].Users ON;

INSERT INTO [feleves].Users(ID, Username, FirstName, LastName, EmailAddr, Telephone) VALUES
(1, 'danielm', 'Daniel', 'Meszaros', 'danielm@example.com', '12301'),
(2, 'alexanderm', 'Alexander', 'Morris', 'alexanderm@example.com', '12302'),
(3, 'tinam', 'Tina', 'Myers', 'tinam@example.com', '12303'),
(4, 'saulb', 'Saul', 'Benjamin', 'saulb@example.com', ''),
(5, 'warrenm', 'Warren', 'Mills', 'warrenm@example.com', '12305'),
(6, 'zahrav', 'Zahra', 'Vincent', 'zahrav@example.com', '12306'),
(7, 'georger', 'George', 'Raymond', 'georger@example.com', ''),
(8, 'adilg', 'Adil', 'Garrison', 'adilg@example.com', '12308');

SET IDENTITY_INSERT [feleves].Users OFF;
SET IDENTITY_INSERT [feleves].Projects ON;

INSERT INTO [feleves].Projects(ID, Owner, Name, Homepage, CreationDate, Parent, Descript, Lang) VALUES
(1, 1, 'IssueTracker', NULL, SYSDATETIME(), NULL, 'Root project', '(none)'),
(2, 1, 'IssueTracker Repository', NULL, SYSDATETIME(), 1, 'Database repository', 'C#'),
(3, 1, 'IssueTracker Logic', NULL, SYSDATETIME(), 1, 'Controller', 'C#'),
(4, 1, 'IssueTracker UI', NULL, SYSDATETIME(), 1, 'User Interface', 'C#'),
(5, 1, 'IssueTracker Testing', NULL, SYSDATETIME(), 1, 'Tests', 'C#'),
(6, 1, 'IssueTracker Web Backend', 'http://www.durgasoft.com/', SYSDATETIME(), 1, 'Web backend', 'Java');

SET IDENTITY_INSERT [feleves].Projects OFF;

INSERT INTO [feleves].Issues(Project, Title, AssignedTo, Solved, Tag, Prio) VALUES
(4, 'Implement password input field', 1, 0, 'UI', 1),
(3, 'Generate statistics about issues per project and per users.', 1, 0, 'Ctrl', 4),
(4, 'Display the statistics mentioned in #3.', 1, 0, 'UI', 3),
(3, 'Implement user statistics.', 2, 0, 'UI', 3),
(5, 'Test user authentication errors.', 2, 1, 'Auth', 2),
(4, 'Display success messagebox when creating a new issue.', 3, 1, 'UI', 1),
(3, 'Implement retrieval of user complaints from backend.', 3, 0, 'Net', 2),
(4, 'Fix keypresses getting stuck in Backbuffer queue.', 1, 0, 'UI', 3),
(4, 'Fix that custom descriptions were not shown for basic types.', 4, 1, 'UI', 4);
INSERT INTO [feleves].Issues(Project, Title, Solved, Tag, Prio) VALUES
(5, 'Create a faulty mode in FakeListRepository to simulate database errors.', 0, 'Testing', 2);
INSERT INTO [feleves].Issues(Project, Title, AssignedTo, Solved, Tag, Prio) VALUES
(5, 'Check consistency in FakeListRepository.', 3, 0, 'Testing', 2),
(3, 'Make web auth testable.', 6, 1, 'Net', 2);
